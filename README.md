# Summary
JH3-AerialCombat is the continuing development of T12 (a prototype which can be found [here](https://gitlab.com/jimmyh3/T12)).

JH3-AC is a full redesign of T12's code base for better maintability and usage of Unity's assets.
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/**
 * Jimmy He
 * JH3 - Aerial Combat
 * 6/15/17
 */

/// <summary>
/// Weapon is used for spawning Projectile objects. Weapon handles the Projectile
/// spawning locations, firing rate, and instantiation.
/// </summary>
[DisallowMultipleComponent]
public class Weapon : MonoBehaviour
{

    // ***************************************************
    //
    //                  Variables
    //  
    // ***************************************************
    
    [Tooltip("Drag the GameObject with the Projectile component here.")]
    public Projectile projectile;

    [Tooltip("Drag the GameObjects representing the spawn locations for the projectiles here.")]
    public List<Transform> projectileSpawnLocations;
    
    [Tooltip("True = Fire projectiles from every given projectile spawn location when able.\n" +
             "False = Fire one projectile at a time while cycling through each given projectile spawn location.")]
    public bool fireOnAllSpawns = true;
    
    [HideInInspector]
    [Tooltip("Enable infinite ammo.")]
    public bool enableInfiniteAmmo = true;

    [HideInInspector]
    [Tooltip("Enable burst fire mode.")]
    public bool enableBurstFire = false;
    
    [HideInInspector]
    [Tooltip("Burst fire settings of this weapon.")]
    public BurstFire burstFireOptions;
    
    [HideInInspector]
    [Tooltip("Ammo settings of this weapon.")]
    public Ammo ammoInfo;

    [SerializeField]
    [Tooltip("The delay in seconds between firing projectiles.")]
    private float _rateOfFire = 3.0f;
    private float nextFire = 0.0f;          // Next time of next available shot.
    private int pslIndex = 0;               // Index to cycle each Weapon's Projectile Spawn Locations.


    // ***** Properties *****

    public GameUnit weaponOwner
    {
        get
        {
            GameUnit owner = GetComponentInParent<GameUnit>();
            return ((owner != null) && (owner.gameObject != this.gameObject)) ? owner : null;
        }
    }

    public float rateOfFire
    {
        get { return _rateOfFire; }
        set { _rateOfFire = Mathf.Clamp(value, 0.0f, Mathf.Infinity); }
    }
    

    // ***************************************************
    //
    //                  Unity & Helper Methods
    //  
    // ***************************************************

    protected void Start()
    {
        OnValidate();
    }

    protected void OnValidate()
    {
        if (projectile == null)
            Debug.LogWarning(this.name + ": PROJECTILE type missing!", this);

        if (projectileSpawnLocations.Count == 0)
            Debug.LogWarning(this.name + ": PROJECTILE SPAWN LOCATIONS none!", this);

        if (weaponOwner == null)
            Debug.LogWarning(this.name + ": This MUST BE A CHILD of another GameObject a.k.a ('weapon wielder')", this);
        
        ammoInfo.currentAmmoCount = ammoInfo.currentAmmoCount;
        ammoInfo.maximumAmmoCount = ammoInfo.maximumAmmoCount;

        burstFireOptions.salvoCount = burstFireOptions.salvoCount;
        burstFireOptions.rateOfSalvo = burstFireOptions.rateOfSalvo;

        rateOfFire = rateOfFire;
    }

    /*Formerly used to give extra speed to Projectiles if Player moves forward
    //protected virtual void UpdateProjectileDeltaSpeed()
    //{
    //    // Determine speed by differering changing positions over time.
    //    deltaSpeed = ((transform.position - prevPosition) / Time.deltaTime).z;

    //    // Only increase projectile speeds if Weapon is moving transform.forward. Never decrease.
    //    if ((deltaSpeed * transform.forward.z) < 0)
    //        deltaSpeed = 0.0f;

    //    // Delta speed is negative if plane is heading toward (0,0,0). Make it positive.
    //    deltaSpeed = Mathf.Abs(deltaSpeed);

    //    // Reset previous Weapon's position to current position.
    //    prevPosition.x = transform.position.x;
    //    prevPosition.y = transform.position.y;
    //    prevPosition.z = transform.position.z;
    //}
    */

    // ***************************************************
    //
    //                  Public Methods
    //  
    // ***************************************************

    public virtual bool canFire()
    {
        bool hasElapsedROFTime  = (Time.time > nextFire);
        bool hasAmmoToShoot     = (ammoInfo.currentAmmoCount > 0 || enableInfiniteAmmo);
        bool hasProjectileSpawn = (projectileSpawnLocations.Count > 0);

        return (this.gameObject.activeSelf && this.enabled && hasElapsedROFTime && hasAmmoToShoot && hasProjectileSpawn);
    }

    /// <summary>Instantiate this Weapon's Projectile at the specified Transform position.</summary>
    /// <param name="spawnTransform">Projectile's spawn location.</param>
    public virtual void Fire()
    {
        if (canFire())
        {
            Fire_HandleAmmoCount();
            Fire_HandleFiringRate();
            Fire_HandleFiring();
        }
    }
    
    // ***************************************************
    //
    //              Private/Protected Methods
    //  
    // ***************************************************

    /// <summary>
    /// Helper method to <see cref="Fire()"/>.
    /// Handle ammo count.
    /// </summary>
    protected virtual void Fire_HandleAmmoCount()
    {
        // Set values for next projectile.
        if (ammoInfo.currentAmmoCount > 0 && !enableInfiniteAmmo)
            ammoInfo.currentAmmoCount--;
    }

    /// <summary>
    /// Helper method to <see cref="Fire()"/>. 
    /// It handles the fire rate and timing the next available shot.
    /// </summary>
    protected virtual void Fire_HandleFiringRate()
    {
        // Determine whether to set next time of firing by burst fire delay or by rate of fire.
        if (enableBurstFire)
        {
            burstFireOptions.salvoCounter++;

            if (burstFireOptions.salvoCounter == 0)
                nextFire = burstFireOptions.rateOfSalvo + Time.time;
            else
                nextFire = rateOfFire + Time.time;
        }
        else
        {
            nextFire = rateOfFire + Time.time;
        }
    }

    /// <summary>
    /// Helper method to <see cref="Fire()"/>.
    /// It handles the firing ports followed by instantiating the Projectiles.
    /// </summary>
    protected virtual void Fire_HandleFiring()
    {
        if (fireOnAllSpawns) // Instantiate Projectile for every spawn at once
        {
            foreach (Transform x in projectileSpawnLocations)
                Fire_HandleInstantiateProjectile(x);
        }
        else                 // Instantiate Projectile per single spawn every firing rate.
        {
            Fire_HandleInstantiateProjectile(projectileSpawnLocations[pslIndex]);
            pslIndex = (pslIndex + 1) % projectileSpawnLocations.Count; // Increment / loop the index.
        }
    }

    /// <summary>
    /// Helper method to <see cref="Fire()"/>.
    /// This method instantiates this Weapon's Projectile, add speed to the Projectile based
    /// on the forward movement of this Weapon/wielder, and set the owner tag of the Projectile.
    /// </summary>
    /// <param name="spawnLocation"></param>
    /// <returns></returns>
    protected virtual Projectile Fire_HandleInstantiateProjectile(Transform spawnLocation)
    {
        Vector3 spawnPosition;
        Quaternion spawnRotation;
        Projectile firedProjectile;
        
        spawnPosition = spawnLocation.position;
        spawnRotation = spawnLocation.rotation;

        firedProjectile = Instantiate(projectile, spawnPosition, spawnRotation);

        firedProjectile.projectileOwner = weaponOwner;

        return firedProjectile;
    }
    

    // ***************************************************
    //
    //                  Helper Classes
    //  
    // ***************************************************
    
    [System.Serializable]
    public class BurstFire
    {
        /*** Variables ***/
        [SerializeField]
        [Tooltip("Fire off this number of times in succession before salvo delay.")]
        private int _salvoCount = 1;

        [SerializeField]
        [Tooltip("Delay in seconds until next salvo can be fired.")]
        private float _rateOfSalvo = 3.0f;

        private int _currentSalvoCount = 0;

        /*** Properties ***/
        public int salvoCount
        {
            get { return _salvoCount; }
            set { _salvoCount = (int)Mathf.Clamp(value, 1, Mathf.Infinity); }
        }

        public float rateOfSalvo
        {
            get { return _rateOfSalvo; }
            set { _rateOfSalvo = Mathf.Clamp(value, 0.0f, Mathf.Infinity); }
        }

        public int salvoCounter
        {
            get { return _currentSalvoCount; }
            set { _currentSalvoCount = value % salvoCount; }
        }
    }

    [System.Serializable]
    public class Ammo
    {
        /*** Variables ***/
        [SerializeField]
        [Tooltip("Current amount of ammo for this weapon.")]
        private int _currentAmmoCount;

        [SerializeField]
        [Tooltip("Maximum amount of ammo for this weapon.")]
        private int _maximumAmmoCount;

        public int currentAmmoCount
        {
            get { return _currentAmmoCount; }
            set { _currentAmmoCount = (int)Mathf.Clamp(value, 0, maximumAmmoCount); }
        }

        public int maximumAmmoCount
        {
            get { return _maximumAmmoCount; }
            set { _maximumAmmoCount = (int)Mathf.Clamp(value, 0, Mathf.Infinity); }
        }
    }



    // ***************************************************
    //
    //                  Custom Inspector
    //  
    // ***************************************************

#if UNITY_EDITOR

    /// <summary>
    /// Custom inspector for Unity in regards to using the <see cref="Weapon"/> class.
    /// </summary>
    [CustomEditor(typeof(Weapon))]
    [CanEditMultipleObjects]
    private class WeaponEditor : Editor
    {
        private Weapon _weaponScript;

        private void OnEnable()
        {
            _weaponScript = (Weapon)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            HandleAmmoInfo(_weaponScript);
            HandleBurstFireOptions(_weaponScript);

            serializedObject.ApplyModifiedProperties();
        }

        private void HandleAmmoInfo(Weapon weaponScript)
        {
            // Display 'Infinite Ammo' boolean check box.
            SerializedProperty enableInfiniteAmmo = serializedObject.FindProperty("enableInfiniteAmmo");
            SerializedProperty ammoInfo = serializedObject.FindProperty("ammoInfo");
            /*** Ammo class variables ***/
            SerializedProperty currentAmmoCount = ammoInfo.FindPropertyRelative("_currentAmmoCount");
            SerializedProperty maximumAmmoCount = ammoInfo.FindPropertyRelative("_maximumAmmoCount");

            EditorGUILayout.PropertyField(enableInfiniteAmmo);
            if (!weaponScript.enableInfiniteAmmo)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(currentAmmoCount);
                EditorGUILayout.PropertyField(maximumAmmoCount);
                EditorGUI.indentLevel--;
            }

        }

        private void HandleBurstFireOptions(Weapon weaponScript)
        {
            SerializedProperty enableBurstFire = serializedObject.FindProperty("enableBurstFire");
            SerializedProperty burstFireOptions = serializedObject.FindProperty("burstFireOptions");
            /*** BurstFire class variables ***/
            SerializedProperty salvoCount = burstFireOptions.FindPropertyRelative("_salvoCount");
            SerializedProperty rateOfSalvo = burstFireOptions.FindPropertyRelative("_rateOfSalvo");

            EditorGUILayout.PropertyField(enableBurstFire);
            if (weaponScript.enableBurstFire)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(salvoCount);
                EditorGUILayout.PropertyField(rateOfSalvo);
                EditorGUI.indentLevel--;
            }
        }

    }

#endif

}

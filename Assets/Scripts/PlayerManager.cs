﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains essentials for the Player him/herself.
/// <para>
/// <see cref="PlayerManager"/> is a Singleton so use
/// <see cref="PlayerManager.Instance"/> to access its properties and functions.
/// </para>
/// </summary>
public class PlayerManager : MonoBehaviour {

    // ****** Delegates/Events ******
    /// <summary>
    /// Method signature used by <see cref="OnPlayerUnitReset"/> to inform
    /// listeners that the <see cref="playerUnit"/> has reset/revived.
    /// </summary>
    public delegate void PlayerResetCallBack();
    /// <summary>
    /// Notifies all interested listeners when <see cref="playerUnit"/> goes to a non-null reference.
    /// </summary>
    public event PlayerResetCallBack OnPlayerUnitReset;
    /// <summary>
    /// Method signature used by <see cref="OnPlayerCurrentScoreModified"/> to inform
    /// listeners that the <see cref="playerCurrentScore"/> has been modified.
    /// </summary>
    /// <param name="value">The current score to pass to listeners.</param>
    public delegate void CurrentScoreCallBack(int value);
    /// <summary>
    /// Notifies all interested listeners when <see cref="playerCurrentScore"/> is modified.
    /// </summary>
    public event CurrentScoreCallBack OnPlayerCurrentScoreModified;

    // *********************
    //
    //      Class Fields
    //
    // *********************
    
    // ****** Static Fields ******
    private static PlayerManager _instance;
    // ****** Public/Inspector Fields ******
    [Tooltip("The default player prefab to instantiate upon respawn.")]
    [SerializeField] private GameUnit _playerUnitPREFAB;
    [Tooltip("The player unit that is on the scene.")]
    [SerializeField] private GameUnit _playerUnitLIVE;
    [SerializeField] private float _playerRespawnTime = 3.0f;
    [SerializeField] private int _playerCurrentScore;
    [SerializeField] private int _playerLivesCount = 3;
    
    // *********************
    //
    //      Properties
    //
    // *********************

    // ***** Static Properties *****
    /// <summary>
    /// The single instance of <see cref="PlayerManager"/> that is live on the scene.
    /// Note that if it does exist on the scene then one will be created and returned.
    /// </summary>
    public static PlayerManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<PlayerManager>();

            if (_instance == null)
            {
                GameObject gobj = new GameObject("PlayerUnitManager");
                _instance = gobj.AddComponent<PlayerManager>();

                Debug.LogWarning(typeof(PlayerManager).Name + " is required but was "
                                        + "missing on the scene. However is has now been created!");
            }

            return _instance;
        }
    }
    
    // ***** Public Properties *****
    
    /// <summary>
    /// The <see cref="GameUnit"/> that is controlled by the Player in the scene.
    /// However note that null may be returned.
    /// </summary>
    public GameUnit playerUnit
    {
        get { return _playerUnitLIVE; }
        set
        {
            _playerUnitLIVE = value;

            if (OnPlayerUnitReset != null)
                OnPlayerUnitReset();
        }
    }

    /// <summary>The current score of the Player.</summary>
    public int playerCurrentScore
    {
        get { return _playerCurrentScore; }
        set
        {
            _playerCurrentScore = Mathf.Clamp(value, 0, int.MaxValue);

            if (OnPlayerCurrentScoreModified != null)
                OnPlayerCurrentScoreModified(_playerCurrentScore);
        }
    }

    /// <summary>The duration to wait until the player respawns again when dead.</summary>
    public float playerRespawnTime
    {
        get { return _playerRespawnTime; }
        set { _playerRespawnTime = Mathf.Clamp(value, 0.0f, float.MaxValue); }
    }


    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    private void OnValidate()
    {
        // Warn with error if multiple instances of Player Manager was found on scene.
        if (FindObjectsOfType<PlayerManager>().Length > 1)
            Debug.LogError(typeof(PlayerManager).Name + ": requires only one instance of itself on the scene!", this);
    }

    private void Start()
    {
        // Find initial Player instance if it exists.
        if (_playerUnitLIVE == null)
        {
            PlayerInput pi = FindObjectOfType<PlayerInput>();
            if (pi != null) _playerUnitLIVE = pi.GetComponent<GameUnit>();
        }
    }

    private void OnApplicationQuit()
    {
        _instance = null;
    }

    // ***********************
    //
    //      Public Methods
    //
    // ***********************

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[DisallowMultipleComponent]
public class PlayerHUDController : MonoBehaviour {

    // **** Public/Inspector Fields ****
    public PlayerManager playerManager;
    public PlayerHUDDisplay playerHUDDisplay;
    [SerializeField] private float _slowOnInputRelease = 1.0f;

    public float slowOnInputRelease
    {
        get { return _slowOnInputRelease; }
        set { _slowOnInputRelease = Mathf.Clamp(value, 0.0f, 1.0f); }
    }

    // ***********************
    //
    //      Unity Method
    //
    // ***********************

    private void OnValidate()
    {
        // Warn with error if multiple instances of Player Manager was found on scene.
        if (FindObjectsOfType<PlayerHUDController>().Length > 1)
            Debug.LogError(typeof(PlayerHUDController).Name + ": requires only one instance on the scene!", this);

        if (playerManager == null)
            playerManager = FindObjectOfType<PlayerManager>();

        if (playerHUDDisplay == null)
            playerHUDDisplay = FindObjectOfType<PlayerHUDDisplay>();

        if (playerManager == null)
            Debug.LogError(this.name + ": " + typeof(PlayerManager).Name + " is missing!", this);

        if (playerHUDDisplay == null)
            Debug.LogError(this.name + ": " + typeof(PlayerHUDDisplay).Name + " is missing!", this);
    }

    private void Start()
    {
        OnValidate(); 
    }

    private void Update()
    {
        HandleOnInputRelease();
        RefreshPlayerHealthBar();
        RefreshPlayerScoreText();
    }

    private void HandleOnInputRelease()
    {
        bool isLeftMouseUp = CrossPlatformInputManager.GetButtonUp(InputJH3.LeftMouseKey);
        bool isLeftMouseDown = CrossPlatformInputManager.GetButtonDown(InputJH3.LeftMouseKey);
        
        // If user's left mouse/finger has lifted from the scene.
        if (isLeftMouseUp)
        {
            GameManagerJH3.PauseGame();

            if (PlayerManager.Instance.playerUnit != null)
                PlayerManager.Instance.playerUnit.GetComponent<PlayerInput>().enabled = false;

            playerHUDDisplay.accessPowersButton.gameObject.SetActive(true);
            playerHUDDisplay.accessPauseButton.gameObject.SetActive(true);
            playerHUDDisplay.onInputReleasePanel.gameObject.SetActive(true);
            playerHUDDisplay.pressedAndReleasedEvents.releasedEvents.Invoke();
        }


        // If user's left mouse/finger is pressed back onto the scene.
        if (isLeftMouseDown && !HasUserClickedInteractable())
        {
            // If playerPowersManager is not null and the Player is NOT current selecting targets...
            GameManagerJH3.UnPauseGame();

            if (PlayerManager.Instance.playerUnit != null)
                PlayerManager.Instance.playerUnit.GetComponent<PlayerInput>().enabled = true;

            playerHUDDisplay.accessPowersButton.gameObject.SetActive(false);
            playerHUDDisplay.accessPauseButton.gameObject.SetActive(false);
            playerHUDDisplay.onInputReleasePanel.gameObject.SetActive(false);
            playerHUDDisplay.pressedAndReleasedEvents.pressedEvents.Invoke();
        }
    }

    /// <summary>
    /// Helper method to determine if the user has clicked on an interactable UI object.
    /// The interactable is based on <see cref="Selectable.interactable"/>, which is
    /// inherited by UI elements such as <see cref="Button"/>.
    /// </summary>
    /// <returns></returns>
    private bool HasUserClickedInteractable()
    {
        bool hasClickedInteractable = false;
        PointerEventData pointerEventData;
        List<RaycastResult> raycastResults;
        pointerEventData = new PointerEventData(null);
        raycastResults = new List<RaycastResult>();


        pointerEventData.position = Input.mousePosition;
        playerHUDDisplay.graphicRaycaster.Raycast(pointerEventData, raycastResults);

        foreach (RaycastResult raycastResult in raycastResults)
        {
            Selectable uiSelectable = raycastResult.gameObject.GetComponent<Selectable>();
            if (uiSelectable != null && uiSelectable.interactable)
            {
                hasClickedInteractable = true;
                break;
            }
        }

        return hasClickedInteractable;
    }

    /// <summary>
    /// Refreshes the <see cref="PlayerHUDDisplay.healthBar"/> by calling the <see cref="PlayerManager.playerUnit"/>
    /// to get its <see cref="UnitCompHealth.currentHealth"/>. The <see cref="PlayerHUDDisplay.healthBar"/> will set its
    /// <see cref="Slider.minValue"/> and <see cref="Slider.maxValue"/> by the <see cref="UnitCompHealth.currentHealth"/> and
    /// <see cref="UnitCompHealth.maximumHealth"/> respectively.
    /// </summary>
    private void RefreshPlayerHealthBar()
    {
        if (playerManager.playerUnit != null)
        {
            GameUnit playerUnit = playerManager.playerUnit;
            UnitCompHealth playerHealthComp = (playerUnit != null) ? playerUnit.GetComponent<UnitCompHealth>() : null;

            if (playerHealthComp != null)
            {
                float currentHealth = playerHealthComp.currentHealth;
                float maximumHealth = playerHealthComp.maximumHealth;

                playerHUDDisplay.healthBar.minValue = 0.0f;
                playerHUDDisplay.healthBar.maxValue = maximumHealth;

                playerHUDDisplay.healthBar.value = currentHealth;
            }
        }
        else
        {
            playerHUDDisplay.healthBar.value = 0;
        }
    }

    /// <summary>
    /// Refreshes the <see cref="PlayerHUDDisplay.scoreText"/> by getting its value from
    /// <see cref="PlayerManager.playerCurrentScore"/>.
    /// </summary>
    private void RefreshPlayerScoreText()
    {
        playerHUDDisplay.scoreText.text = "Score: " + playerManager.playerCurrentScore;
    }

}

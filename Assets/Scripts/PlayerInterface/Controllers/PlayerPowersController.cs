﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class PlayerPowersController : MonoBehaviour {

    // ****** public fields ******
    public PowersManager ppm;
    public PlayerPowersDisplay ppd;

    // ****** private fields ******
    private Button currentSelectedPowerButton;

    // ****** properties ******
    private GameUnit Player { get { return PlayerManager.Instance.playerUnit; }}

    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    private void OnValidate()
    {
        if (ppm == null)
            ppm = GameObject.FindObjectOfType<PowersManager>();

        if (ppd == null)
            ppd = GameObject.FindObjectOfType<PlayerPowersDisplay>();

        if (ppm == null)
            Debug.LogError(this.name + ": playerPowersManager is missing!");

        if (ppd == null)
            Debug.LogError(this.name + ": playersPowersDisplay is missing!");
    }

    private void Start()
    {
        OnValidate();
        currentSelectedPowerButton = null;
        Start_PlayerPowerDisplayButtons();
    }

    private void Start_PlayerPowerDisplayButtons()
    {
        // Rig each button to act accordingly.
        foreach (Button powerButton in ppd.powerButtonsDisplay.GetComponentsInChildren<Button>(true))
        {
            PowerReference ppr = powerButton.GetComponentInChildren<PowerReference>(true);
            if (ppr != null)
            {
                powerButton.onClick.RemoveAllListeners();
                powerButton.onClick.AddListener(delegate { PressedPowerButton(powerButton); });
            }
        }

        //ppd.backButton.onClick.RemoveAllListeners();
        //ppd.backButton.onClick.AddListener(PressedAccessPowersButton);

        ppd.activatePowerButton.onClick.RemoveAllListeners();
        ppd.activatePowerButton.onClick.AddListener(PressedActivatePowerButton);

        ppd.cancelButton.onClick.RemoveAllListeners();
        ppd.cancelButton.onClick.AddListener(PressedCancelButton);
        // -----------------------------------
    }

    private void OnGUI()
    {
        if (currentSelectedPowerButton == null) return;

        PowerReference ppr;
        AbstractPower app;

        if (HasPprAndApp(currentSelectedPowerButton.gameObject, out ppr, out app))
        {
            List<GameUnit> potentialTargets = null;
            int numOfTargets = 0;

            if (app.TryGetPotentialTargets(Player, out potentialTargets, out numOfTargets))
            {
                foreach (GameUnit potentialTarget in potentialTargets)
                {
                    /*
                     * Used for GUI.Button() to draw rectangle on a target.
                     */
                    Rect guiRectCovering;   // The Rect encompassing the GameUnit on the screen to show it's targeted.
                    GUIContent guiContent;  // The GUIContent holds the image/text/color of the GUIButton later.
                    GUIStyle guiStyle;      // The GUIStyle is the styling of the aforementioned GUIContent.

                    guiContent = new GUIContent();
                    guiStyle = new GUIStyle();

                    guiRectCovering = GUIJH3.GetGUIRectCovering(potentialTarget);
                    guiStyle.stretchHeight = true;
                    guiStyle.stretchWidth = true;

                    // Return as the object is either behind screen view or it has no bounds.
                    if (guiRectCovering.width == 0.0f && guiRectCovering.height == 0.0f) return;


                    // Determine if the list already contains this possible target.
                    int tguIndex = app.targetedUnits.IndexOf(potentialTarget);
                    guiContent.image = (tguIndex < 0) ? ppd.drawTextures.isTargetableRect : ppd.drawTextures.isTargetedRect;

                    /**
                     * Highlight <see cref="possibleTarget"/> by a given <see cref="isTargetableRect"/> or <see cref="isTargetedRect"/>
                     * depending on whether or not the current <see cref="possibleTarget"/> is in the <see cref="targetedUnits"/> list.
                     * The highlights are selectable <see cref="GUI.Button(Rect, GUIContent, GUIStyle)"/>s that allows the user to add or remove them.
                     */
                    if (tguIndex < 0)
                    {
                        if (GUI.Button(guiRectCovering, guiContent, guiStyle))
                            if (app.targetedUnits.Count < numOfTargets)
                                app.targetedUnits.Add(potentialTarget);
                    }
                    else
                    {
                        if (GUI.Button(guiRectCovering, guiContent, guiStyle))
                            app.targetedUnits.RemoveAt(tguIndex);
                    }
                }
            }
        }
    }

    private void OnEnable()
    {
        RefreshDisplayPowerButtons();
    }


    // ***********************
    //
    //      Private Methods
    //
    // ***********************

    /// <summary>
    /// The <see cref="Button.onClick"/> action for <see cref="PlayerPowersDisplay.backButton"/>.
    /// </summary>
    private void RefreshDisplayPowerButtons()
    {
        if (ppd == null) return;

        // Display all power buttons (disable those that are still in activated state)
        RefreshPowerInteractableButtons(ppd.powerButtonsDisplay, true);
        // Disable accessPowersButton and enable cancelButton
        //ppd.ToggleActiveDisplayItems(1, 0, -1, 1);
        ppd.ToggleActiveDisplayItems(1, 1, 0, 0);
    }

    /// <summary>
    /// Hides all <see cref="AbstractPower"/> <see cref="Button"/>s in <see cref="ppd"/>
    /// except itself and sets the <see cref="currentSelectedPowerButton"/> to itself.
    /// <para>
    /// Note that the <see cref="Button"/> given must also have a <see cref="PowerReference"/>
    /// otherwise it will return and do nothing.
    /// </para>
    /// </summary>
    /// <param name="_powerButton"></param>
    private void PressedPowerButton(Button _powerButton)
    {
        if (ppd == null) return;
        if (currentSelectedPowerButton == _powerButton) return; // Do nothing if selected same power button.
        
        PowerReference ppr;
        AbstractPower app;
        
        if (HasPprAndApp(_powerButton.gameObject, out ppr, out app))
        {
            if (app.isActivated) return;

            // Unhide the Activate button.
            ppd.ToggleActiveDisplayItems(-1, -1, 1, -1);
            // Hide the Back button and reveal Cancel Button.
            ppd.ToggleActiveDisplayItems(-1, 0, -1, 1);
            // Hide all power buttons.
            ppd.ToggleActiveDisplayItems(0, -1, -1, -1);
            // Unhide the currently selected power button.
            _powerButton.gameObject.SetActive(true);
            _powerButton.interactable = false;

            // Set power targeting state to true.
            //app.isTargeting = true;

            currentSelectedPowerButton = _powerButton;
        }
    }

    /// <summary>
    /// The <see cref="Button.onClick"/> action for <see cref="PlayerPowersDisplay.activatePowerButton"/>.
    /// </summary>
    private void PressedActivatePowerButton()
    {
        if (ppd == null) return;
        
        PowerReference ppr;
        AbstractPower app;
        string appConstraints;

        if (currentSelectedPowerButton == null) return;
        
        if (HasPprAndApp(currentSelectedPowerButton.gameObject, out ppr, out app))
        {
            // Activate power if possible.
            if (app.TryActivatingAffect(Player, out appConstraints))
            {
                // Successful activation.

                // Disable the 'Activate' button.
                ppd.ToggleActiveDisplayItems(-1, -1, 0, -1);
                ppd.FlashAlphaText(appConstraints, 255.0f, 3.0f, true);

                // Set Active all power buttons to view them again.
                ppd.ToggleActiveDisplayItems(1, -1, -1, -1);
                // Hide Cancel button and reveal Back button
                ppd.ToggleActiveDisplayItems(-1, 1, -1, 0);

                //app.isTargeting = false;
                currentSelectedPowerButton.interactable = false;
                currentSelectedPowerButton = null;

                // Refresh interactable buttons in the list.
                RefreshPowerInteractableButtons(ppd.powerButtonsDisplay, true);
            }
            else
            {
                // Failed activation.
                ppd.FlashAlphaText(appConstraints, 255.0f, 3.0f, true);
            }
        }
    }

    /// <summary>The <see cref="Button.onClick"/> action for <see cref="PlayerPowersDisplay.cancelButton"/>.</summary>
    private void PressedCancelButton()
    {
        if (ppd == null) return;
        
        // Handle cases of pressing the cancel button.
        if (currentSelectedPowerButton != null)
        {
            // Case: pressed cancel during targeting power.
            PowerReference ppr;
            AbstractPower app;

            if (HasPprAndApp(currentSelectedPowerButton.gameObject, out ppr, out app))
            {
                //app.isTargeting = false;
                app.targetedUnits.Clear();
            }

            currentSelectedPowerButton.interactable = true;
            currentSelectedPowerButton = null;
            //ppd.ToggleActiveDisplayItems(1, -1, 0, -1);
            ppd.ToggleActiveDisplayItems(1, 1, 0, 0);
        }
        //else
        //{
        //    // Case: if going back to the OnInputReleasePanel
        //    ppd.ToggleActiveDisplayItems(0, 1, -1, 0);
        //}
    }

    /// <summary>
    /// For every <see cref="Button"/> that is a child in the given <see cref="RectTransform"/>,
    /// determine its <see cref="Selectable.interactable"/> state. Its state is determined by
    /// its <see cref="PowerReference"/> component (if it has one) and the accompanying
    /// <see cref="AbstractPower"/> referenced within. If the <see cref="AbstractPower.isActivated"/>
    /// is true, then the <see cref="Selectable.interactable"/> state is true, otherwise, it's false.
    /// </summary>
    /// <param name="powerButtonsDisplay">The <see cref="RectTransform"/> to get <see cref="Button"/>s from.</param>
    /// <param name="allButtonStates">The <see cref="GameObject.SetActive(bool)"/> to set all <see cref="Button"/>s found.</param>
    private void RefreshPowerInteractableButtons(RectTransform powerButtonsDisplay, bool allButtonStates)
    {
        if (powerButtonsDisplay == null) return;
        
        RefreshPowerInteractableButtons(powerButtonsDisplay.GetComponentsInChildren<Button>(true), allButtonStates);
    }

    /// <summary>
    /// For every <see cref="Button"/> determine its <see cref="Selectable.interactable"/> state. 
    /// Its state is determined by its <see cref="PowerReference"/> component (if it has one) and the accompanying
    /// <see cref="AbstractPower"/> referenced within. If the <see cref="AbstractPower.isActivated"/>
    /// is true, then the <see cref="Selectable.interactable"/> state is true, otherwise, it's false.
    /// </summary>
    /// <param name="powerButtons">The <see cref="Button"/>s to set <see cref="Selectable.interactable"/>.</param>
    /// <param name="allButtonStates">The <see cref="GameObject.SetActive(bool)"/> to set all <see cref="Button"/>s found.</param>
    private void RefreshPowerInteractableButtons(Button[] powerButtons, bool allButtonStates)
    {
        if (powerButtons == null || powerButtons.Length == 0) return;

        // Display all power buttons (disable those that are still in activated state)
        foreach (Button powerButton in powerButtons)
        {
            PowerReference ppr;
            AbstractPower app;
            
            if (HasPprAndApp(powerButton.gameObject, out ppr, out app))
            {
                // Disable this button if the current power is activated otherwise enable it.
                if (app.isActivated)
                    powerButton.interactable = false;
                else
                    powerButton.interactable = true;

                powerButton.gameObject.SetActive(allButtonStates);
            }
        }
    }

    /// <summary>
    /// Determines if the given <see cref="PowerReference"/> and <see cref="AbstractPower"/>
    /// exists. The <see cref="GameObject"/> container is used to find the <see cref="PowerReference"/>, 
    /// and the <see cref="AbstractPower"/> is found by referencing the <see cref="PowerReference.power"/>.
    /// </summary>
    /// 
    /// <param name="container">The <see cref="GameObject"/> container to find <see cref="PowerReference"/>.</param>
    /// <param name="_ppr">The returning <see cref="PowerReference"/>.</param>
    /// <param name="_app">The returning <see cref="AbstractPower"/>.</param>
    /// 
    /// <returns>
    /// <see cref="bool"/> indicating if both <see cref="out"/> <see cref="PowerReference"/> and
    /// <see cref="out"/> <see cref="AbstractPower"/> are NOT <see cref="null"/>.
    /// </returns>
    private bool HasPprAndApp(GameObject container, out PowerReference _ppr, out AbstractPower _app)
    {
        bool hasPprAndApp = false;

        _ppr = (container != null) ? container.GetComponentInChildren<PowerReference>(true) : null;
        _app = (_ppr != null) ? _ppr.power : null;
        
        if (_ppr == null)
            Debug.LogError(this.name + ": player power reference missing!", this);

        if (_app == null)
            Debug.LogError(this.name + ": player power is missing!", this);

        hasPprAndApp = (_ppr != null && _app != null);

        return hasPprAndApp;
    }
}

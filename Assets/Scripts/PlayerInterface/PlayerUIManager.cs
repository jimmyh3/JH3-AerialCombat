﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerUIManager : MonoBehaviour
{
    [Tooltip("Drag the initial Canvas of the current scene here."
                + " For example the main menu would be the first to show upon initialization.")]
    public Canvas initialMenu;
    private Canvas currentMenu;
    private GameManagerJH3 gameController;

    // ****************
    //
    //  Unity Methods
    //
    // ****************

    public void OnValidate()
    {
        // Warn with error if multiple instances of Player Manager was found on scene.
        if (FindObjectsOfType<PlayerUIManager>().Length > 1)
            Debug.LogError(typeof(PlayerUIManager).Name + ": requires only once instance of itself on the scene!", this);
    }
    
    private void Start()
    {
        currentMenu = initialMenu;
        gameController = GameObject.FindObjectOfType<GameManagerJH3>();
    }

    /// <summary>
    /// Opens the given Canvas object by setting its active state to true
    /// and setting the current Canvas state to false.
    /// </summary>
    /// <param name="newMenu">Canvas menu object</param>
    public void OpenMenu(Canvas newMenu)
    {
        if (currentMenu == newMenu)
        {
            currentMenu.gameObject.SetActive(true);
            return;
        }

        //CloseCurrentMenu()
        if (currentMenu != null)
            currentMenu.gameObject.SetActive(false);

        currentMenu = newMenu;
        currentMenu.gameObject.SetActive(true);

    }

    /// <summary>
    /// Close the current Canvas menu by setting its active state to false.
    /// The reference to the closed menu will be null.
    /// </summary>
    public void CloseCurrentMenu()
    {
        if (currentMenu != null)
        {
            currentMenu.gameObject.SetActive(false);
            currentMenu = null;
        }
    }

    // *** Note that these are wrapper methods as UnityEngine.UI.Button cannot call static methods. ***

    /// <summary>
    /// Resumes by calling the static method <see cref="GameManagerJH3.UnPauseGame"/>.
    /// </summary>
    public void UnPauseGame()
    {
        GameManagerJH3.UnPauseGame();
    }

    /// <summary>
    /// Quits the game by calling the static method <see cref="GameManagerJH3.QuitGame"/>.
    /// </summary>
    public void QuiteGame()
    {
        GameManagerJH3.QuitGame();
    }

}

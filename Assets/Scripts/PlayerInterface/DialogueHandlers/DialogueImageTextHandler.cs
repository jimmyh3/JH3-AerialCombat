﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueImageTextHandler : MonoBehaviour
{
    public Canvas dialogueCanvas;          // Main container of other UI elements of this class.
    public RectTransform dialoguePanel;    // Used as a screent to block input.
    public Image dialogueImage;            // The left image used to display who's talking.
    public Text dialogueText;              // The dialogue box.

}

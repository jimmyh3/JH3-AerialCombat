﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq.Expressions;

/// <summary>
/// <see cref="PlayerPowersDisplay"/> manages what <see cref="AbstractPower"/> can be 
/// targeting at the current moment. It does not handle the power's usage or applications.
/// </summary>
[DisallowMultipleComponent]
public class PlayerPowersDisplay : MonoBehaviour {

    // ****** Class Fields ******
    [Tooltip("The display listing selectable powers for the Player to use.")]
    public RectTransform powerButtonsDisplay;
    [Tooltip("The button to activate the currently selected power.")]
    public Button activatePowerButton;
    [Tooltip("The button to cancel the currently selected power.")]
    public Button cancelButton;
    [Tooltip("The button to go back to the previous menu, which is to be rigged via the Inspector.")]
    public Button backButton;
    [Tooltip("The text display to provide the user instructive messages.")]
    public Text messageText;
    [Tooltip("Textures to be used to be drawn onto the screen.")]
    public DrawTextures drawTextures;

    /// <summary>
    /// <see cref="Coroutine"/> to run <see cref="FlashAlphaText(Text, float, float, bool)"/>.
    /// </summary>
    private IEnumerator flashAlphaCoroutine;

    /// <summary>
    /// Tracks the desired highlighted <see cref="Button"/>s' original <see cref="ColorBlock"/>
    /// and altered <see cref="ColorBlock"/> so that it may be switched back.
    /// </summary>
    private Dictionary<int, KeyValuePair<ColorBlock, ColorBlock>> buttonColorBlockDict;

    /// <summary>
    /// Check if the <see cref="backButton"/> is a opened.
    /// </summary>
    //public bool isOpened { get; private set; }
    

    // **************************
    //
    //      Unity Methods
    //
    // **************************

    private void Awake()
    {
        if (powerButtonsDisplay == null)
            Debug.LogError(this.name + ": powersDisplay is missing!", this);

        if (backButton == null)
            Debug.LogError(this.name + ": powersButton is missing!", this);

        if (activatePowerButton == null)
            Debug.LogError(this.name + ": activateButton is missing", this);

        if (cancelButton == null)
            Debug.LogError(this.name + ": cancelButton is missing!", this);

        if (messageText == null)
            Debug.LogError(this.name + ": messageText is missing!", this);

        if (drawTextures.isTargetedRect == null)
            Debug.LogWarning(this.name + ": drawTextures.isTargetedRect is missing!", this);

        if (drawTextures.isTargetableRect == null)
            Debug.LogWarning(this.name + ": drawTextures.isTargetableRect is missing!", this);

        buttonColorBlockDict = new Dictionary<int, KeyValuePair<ColorBlock, ColorBlock>>();

        ToggleActiveDisplayItems(-1, 1, 0, 0);
    }

    // **************************
    //
    //      Public Methods
    //
    // **************************

    /// <summary>
    /// Toggles <see cref="PlayerPowersDisplay"/>'s <see cref="UnityEngine.UI"/>'s 
    /// <see cref="GameObject.SetActive(bool)"/> states of its fields/properties.
    /// 
    /// <para>0 == set <see cref="GameObject.SetActive(bool)"/> to false.</para>
    /// <para>1 == set <see cref="GameObject.SetActive(bool)"/> to true.</para>
    /// <para>-1 == do not modify its current <see cref="GameObject.SetActive(bool)"/>.</para>
    /// 
    /// </summary>
    /// <param name="_isActiveAllPowerButtons">Status for all <see cref="Button"/>s in <see cref="powerButtonsDisplay"/>.</param>
    /// <param name="_isActiveBackButton">Status for <see cref="backButton"/>.</param>
    /// <param name="_isActiveActivatePowerButton">Status for <see cref="activatePowerButton"/>.</param>
    /// <param name="_isActiveCancelButton">Status for <see cref="cancelButton"/>.</param>
    public void ToggleActiveDisplayItems(int _isActiveAllPowerButtons, int _isActiveBackButton,
                                                    int _isActiveActivatePowerButton, int _isActiveCancelButton)
    {
        if (powerButtonsDisplay != null)
        {
            powerButtonsDisplay.gameObject.SetActive(true); // An empty panel will cover nothing so have it true anyway.
            if (_isActiveAllPowerButtons > -1)
                foreach (Button powerButton in powerButtonsDisplay.GetComponentsInChildren<Button>(true))
                    powerButton.gameObject.SetActive(System.Convert.ToBoolean(_isActiveAllPowerButtons));
        }

        if (backButton != null)
            if (_isActiveBackButton > -1)
                backButton.gameObject.SetActive(System.Convert.ToBoolean(_isActiveBackButton));

        if (activatePowerButton != null)
            if (_isActiveActivatePowerButton > -1) 
            activatePowerButton.gameObject.SetActive(System.Convert.ToBoolean(_isActiveActivatePowerButton));

        if (cancelButton != null)
            if (_isActiveCancelButton > -1)
                cancelButton.gameObject.SetActive(System.Convert.ToBoolean(_isActiveCancelButton));

        // Set other fields.
        //isOpened = !accessPowersButton.gameObject.activeSelf;
    }

    /// <summary>
    /// Highlights the given <see cref="Button"/> by changing its <see cref="ColorBlock.normalColor"/>
    /// to its <see cref="ColorBlock.highlightedColor"/> when the given <see cref="bool"/> value is true.
    /// If the given <see cref="bool"/> is false, the <see cref="ColorBlock"/> will be reverted back.
    /// </summary>
    /// <param name="button">The <see cref="Button"/> to highlight.</param>
    /// <param name="toHighlight">Determines whether to highlight the <see cref="Button"/> or not.</param>
    public void ToggleHighlightButton(Button button, bool toHighlight)
    {
        KeyValuePair<ColorBlock, ColorBlock> keyValuePair; // Key = original, Value = highlighted

        if (toHighlight)
        {
            // Highlight only if this button has not been added to the highlighted dictionary before.
            if (!buttonColorBlockDict.TryGetValue(button.GetInstanceID(), out keyValuePair))
            {
                ColorBlock highlightedBlock = new ColorBlock();
                highlightedBlock.normalColor = button.colors.highlightedColor;
                highlightedBlock.highlightedColor = button.colors.highlightedColor;
                highlightedBlock.pressedColor = button.colors.pressedColor;
                highlightedBlock.disabledColor = button.colors.disabledColor;

                keyValuePair = new KeyValuePair<ColorBlock, ColorBlock>(button.colors, highlightedBlock);

                buttonColorBlockDict.Add(button.GetInstanceID(), keyValuePair);
            }
        }
        else
        {
            // Revert highlight only if this button has been added to the highlighted dictionary before.
            if (buttonColorBlockDict.TryGetValue(button.GetInstanceID(), out keyValuePair))
            {
                button.colors = keyValuePair.Key;
                buttonColorBlockDict.Remove(button.GetInstanceID());
            }
        }
    }

    /// <summary>
    /// Flashes the text using <see cref="PlayerPowersDisplay.messageText"/> and its <see cref="Color.a"/>,
    /// which is its alpha color. After the duration ends the <see cref="Color.a"/> will be reset to its original value.
    /// </summary>
    /// <param name="textToDisplay">The <see cref="string"/> to display.</param>
    /// <param name="flashRate">The rate of flashing its alpha per second. This is considered on a 0 to 255 scale.</param>
    /// <param name="duration">The duration of flashing before ending.</param>
    /// <param name="disableAtEnd">Should the textobject be disabled at the end?</param>
    public void FlashAlphaText(string textToDisplay, float flashRate, float duration, bool disableAtEnd)
    {
        if (messageText == null)
        {
            Debug.LogError(this.name + ": messageText is missing!", this);
            return;
        }

        if (textToDisplay.Equals("")
                || Mathf.Approximately(flashRate, 0.0f)
                    || Mathf.Approximately(duration, 0.0f)) return;

        // Stop previous Coroutine if it was called before.
        if (flashAlphaCoroutine != null)
            StopCoroutine(flashAlphaCoroutine);


        messageText.gameObject.SetActive(true);
        messageText.text = textToDisplay;

        flashAlphaCoroutine = FlashAlphaText(messageText, flashRate, duration, disableAtEnd);
        
        StartCoroutine(flashAlphaCoroutine);
    }

    // **************************
    //
    //      Private Methods
    //
    // **************************

    private IEnumerator FlashAlphaText(Text textObject, float flashRate, float duration, bool disableAtEnd)
    {
        float timeElapsed = 0.0f;
        float savedAlphaInc255 = 0.0f;

        float r, g, b, a;
        r = textObject.color.r;
        g = textObject.color.g;
        b = textObject.color.b;
        a = textObject.color.a;
        savedAlphaInc255 = textObject.color.a * 255.0f;

        textObject.gameObject.SetActive(true);
        
        // Flash the alpha of the text object for the given duration by the given rate.
        while (timeElapsed < duration)
        {
            savedAlphaInc255 += (flashRate * Time.unscaledDeltaTime);

            textObject.color = new Color(r, g, b, (Mathf.PingPong(savedAlphaInc255, 255.0f)/255.0f) );
            
            timeElapsed += Time.unscaledDeltaTime;
            yield return null;
        }

        textObject.color = new Color(r, g, b, a);
        textObject.gameObject.SetActive(!disableAtEnd); //disable if requested.
    }
    
    
    // *************************
    //
    //      Private Class
    //
    // *************************

    [System.Serializable]
    public class DrawTextures
    {
        [Tooltip("Texture to draw onto objects that can be targeted during targeting phase.")]
        public Texture isTargetableRect;
        [Tooltip("Texture to draw onto objects that has been targeted during targeting phase.")]
        public Texture isTargetedRect;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// <see cref="PlayerHUDDisplay"/> is a UI handler for the heads up display seen while in a 
/// level. It manages certain transitions that may be dependent on user input and current actions.
/// </summary>
[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(GraphicRaycaster))]
public class PlayerHUDDisplay : MonoBehaviour {

    // **** public/Inspector fields ****
    [Tooltip("Health bar slider indicating how much HP the user has remaining.")]
    public Slider healthBar;
    [Tooltip("The score text display how much the user has received.")]
    public Text scoreText;
    [Tooltip("Button to view Player powers.")]
    public Button accessPowersButton;
    [Tooltip("Button to view the Pause menu.")]
    public Button accessPauseButton;
    [Tooltip("The panel to fade in when the user lifts his/her finger from the screen"
                + " and the events to play upon lifting his/her finger.")]
    public RectTransform onInputReleasePanel;
    [Tooltip("The events to call when onInputReleasePanel is pressed and released.")]
    public PressedAndReleaseEvents pressedAndReleasedEvents;
    
    // ***** private fields *****
    private GraphicRaycaster _graphicRaycaster;

    // ***** Properties *****
    public GraphicRaycaster graphicRaycaster
    {
        get
        {
            if (_graphicRaycaster == null)
                _graphicRaycaster = GetComponent<GraphicRaycaster>();

            return _graphicRaycaster;
        }
    }



    // **********************
    //
    //      Unity Methods
    //
    // **********************

    private void OnValidate()
    {
        // Warn with error if multiple instances of Player Manager was found on scene.
        if (FindObjectsOfType<PlayerHUDDisplay>().Length > 1)
            Debug.LogError(typeof(PlayerHUDDisplay).Name + ": requires only one instance on the scene!", this);

        if (healthBar == null)
            Debug.LogError(this.name + ": healthBar is missing!", this);

        if (scoreText == null)
            Debug.LogError(this.name + ": scoreText is missing!", this);

        if (accessPowersButton == null)
            Debug.LogError(this.name + ": accessPowersButton is missing!", this);

        if (accessPauseButton == null)
            Debug.LogError(this.name + ": accessPauseButton is missing!", this);
        
        if (onInputReleasePanel == null)
            Debug.LogError(this.name + ": onInputReleasePanel is missing!", this);

    }

    private void Awake()
    {
        OnValidate();

        accessPowersButton.gameObject.SetActive(false);
        accessPauseButton.gameObject.SetActive(false);
        onInputReleasePanel.gameObject.SetActive(false);

        _graphicRaycaster = GetComponent<GraphicRaycaster>();
    }

    [System.Serializable]
    public class PressedAndReleaseEvents
    {
        public UnityEvent pressedEvents;
        public UnityEvent releasedEvents;
    }

}

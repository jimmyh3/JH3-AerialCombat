﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DigitalRuby.LightningBolt;
using System;

public class LevelManager01 : AbstractLevelManager {

    // ***********************
    //
    //      Variables
    //
    // ***********************
    
    [SerializeField]
    [Tooltip("Drag the DialogueImageTextHandler here.")]
    private DialogueImageTextHandler dialogueImageTextHandler;

    private int cine01Hash = Animator.StringToHash("Level01Cine01");


    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    private void OnValidate()
    {
        if (dialogueImageTextHandler == null)
            Debug.LogWarning(this.GetType().Name + ": dialogueImageTextHandler is null!", this);

    }

    private void Start()
    {
        OnValidate();
    }
    
    // **********************************************
    //
    //      Cinematic/Animation Related Methods
    //
    // **********************************************
    
    private void CineSetCenterDialogueImage(Image image)
    {
        dialogueImageTextHandler.dialogueImage = image;
    }

    private void CineSetCenterDialogueText(string text)
    {
        dialogueImageTextHandler.dialogueText.text = text;
    }

    private void CineOpenCenterDialogue()
    {
        dialogueImageTextHandler.gameObject.SetActive(true);
    }

    private void CineCloseCenterDialogue()
    {
        dialogueImageTextHandler.gameObject.SetActive(false);
    }

}


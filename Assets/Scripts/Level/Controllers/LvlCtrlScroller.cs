﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlCtrlScroller : MonoBehaviour {

    // ***** Public/Inspector Fields *****
    [SerializeField]
    [Tooltip("The speed of which to scroll the level along.")]
    private float _lvlScrollSpeed;
    [SerializeField]
    [Tooltip("The direction of which to scroll the level. The value will be normalized.")]
    private Vector3 _lvlScrollDirection = new Vector3(0.0f, 0.0f, 1.0f);

    // ***** Properties *****
    public float lvlScrollingSpeed
    {
        get { return _lvlScrollSpeed; }
        set { _lvlScrollSpeed = Mathf.Clamp(value, 0.0f, float.MaxValue); }
    }

    public Vector3 lvlScrollingDirection
    {
        get { return _lvlScrollDirection; }
        set { _lvlScrollDirection = value.normalized; }
    }


    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    private void Update()
    {
        transform.position += lvlScrollingDirection * lvlScrollingSpeed * Time.deltaTime;
    }
}

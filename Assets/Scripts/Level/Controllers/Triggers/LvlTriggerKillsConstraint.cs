﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlTriggerKillsConstraint : MonoBehaviour {


    // ***** Public Fields *****
    /// <summary>
    /// <see cref="GameUnit"/>s that must be destroyed/disable before this
    /// <see cref="LvlTriggerLoad"/> continues scrolling.
    /// </summary>
    [Tooltip("All the units that must be destroyed/disabled before level scrolling.")]
    public List<GameUnit> unitsToDieToContinue;
    // ***** Private Fields *****
    private float listProcessDelay = 1.0f;
    private bool allUnitsDead = false;

    // ***** Properties ******


    // ************************
    //
    //      Unity Methods
    //
    // ************************

    //protected override void OnValidate()
    //{
    //    base.OnValidate();

    //    if (lvlSectionContainer == null)
    //        Debug.LogWarning(this.GetType().Name + ": level container to scroll is missing!", this);
    //}

    private void Start()
    {
        StartCoroutine(CheckAllUnitsDead(unitsToDieToContinue));
    }

    private void Update()
    {
        // allUnitsDead set by local Coroutine
        if (allUnitsDead)
        {
            //lvlSectionContainer.position += new Vector3(0.0f, 0.0f, lvlSectionScrollingSpeed * Time.deltaTime);
        }
    }

    private IEnumerator CheckAllUnitsDead(List<GameUnit> gameUnits)
    {
        while (!allUnitsDead)
        {
            allUnitsDead = AreAllDead(gameUnits);
            yield return new WaitForSeconds(listProcessDelay);
        }
    }
    
    private bool AreAllDead(List<GameUnit> gameUnits)
    {
        bool areAnyAlive = false;

        foreach (GameUnit gameUnit in gameUnits)
        {
            if (gameUnit != null && gameUnit.gameObject.activeSelf)
            {
                areAnyAlive = true;
                break;
            }
        }

        return !areAnyAlive;
    }

}

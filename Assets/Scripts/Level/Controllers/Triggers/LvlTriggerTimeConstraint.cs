﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlTriggerTimeConstraint : MonoBehaviour {

    [Tooltip("The duration to wait until continuing to scroll the level.")]
    public float waitDuration;
    private bool hasWaited = false;

    private void Start()
    {
        StartCoroutine(WaitDuration(waitDuration));
    }

    private IEnumerator WaitDuration(float duration)
    {
        yield return new WaitForSeconds(duration);

        hasWaited = true;
    }
}

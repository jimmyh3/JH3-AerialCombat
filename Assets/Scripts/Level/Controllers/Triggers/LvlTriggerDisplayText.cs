﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
[RequireComponent(typeof(TextJH3))]
[DisallowMultipleComponent]
public class LvlCtrlTriggerDisplayTex : MonoBehaviour {

    // ***** Public/Inspector Fields *****
    [Tooltip("The series of text to display one after another after each duration passes.")]
    public List<string> texts;
    [Tooltip("Rate of the changing the text's alpha.")]
    public float flashRate;
    [Tooltip("Duration of flashing for each text.")]
    public float duration = 3;
    [Tooltip("Disable the text at the end of displaying all texts.")]
    public bool disableAtEnd = true;
    
    // ***** Private Fields *****
    [SerializeField] private TextJH3 textJH3;
    

    // ************************
    //
    //      Unity Methods
    //
    // ************************

    private void OnValidate()
    {
        if (textJH3 == null)
            Debug.LogError(this.GetType().Name + ": TextJH3 component is missing!", this);
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(DisplayTexts());
    }

    private IEnumerator DisplayTexts()
    {
        textJH3.enabled = true;

        foreach (string text in texts)
        {
            if (textJH3 != null)
            {
                textJH3.FlashAlphaText(text, flashRate, duration);
                yield return new WaitForSeconds(duration);
            }
        }

        if (disableAtEnd)
            textJH3.enabled = false;

        yield break;
    }

}

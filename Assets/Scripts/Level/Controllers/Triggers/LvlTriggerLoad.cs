﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class LvlTriggerLoad : MonoBehaviour, IColliderParent {

    // ***** Public/Inspector Fields *****
    [Tooltip("The start trigger to handle initialization events when the Player passes through.")]
    public Collider startTrigger;
    [Tooltip("The end trigger to end any events/de-load objects/load the next level section.")]
    public Collider endTrigger;
    [Tooltip("The GameObject/Container of this section's level items to load upon Player entering the start trigger."
                    + "Likewise, this is also deactivated when Player enters the exit trigger.")]
    public GameObject onStartActivate;
    
    // ***** Unity Methods *****
    protected virtual void OnValidate()
    {
        if (startTrigger == null && endTrigger == null)
            Debug.LogWarning(this.GetType().Name + ": both load sections are null!", this);

        if (startTrigger != null && startTrigger.GetComponent<ColliderChild>() == null)
            startTrigger.gameObject.AddComponent<ColliderChild>();
        
        if (endTrigger != null && endTrigger.GetComponent<ColliderChild>() == null)
            endTrigger.gameObject.AddComponent<ColliderChild>();

    }

    protected virtual void Awake()
    {
        OnValidate();

        if (onStartActivate != null)
            onStartActivate.SetActive(false);
        
    }

    public virtual void ColliderChildEnter(Collider child, Collider other)
    {
        if (child == startTrigger || child == endTrigger)
        {
            if (onStartActivate != null)
                onStartActivate.SetActive(true);
        }
    }

    public virtual void ColliderChildExit(Collider child, Collider other)
    {
        if (child == endTrigger)
        {
            if (onStartActivate != null)
                onStartActivate.SetActive(false);
        }
    }
    
}

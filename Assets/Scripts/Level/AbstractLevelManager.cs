﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class AbstractLevelManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /// <summary>
    /// <see cref="AbstractLevelAct"/> is meant to be implemented by <see cref="AbstractLevelManager"/>
    /// and its main purpose is to handle cinematic events in the level.
    /// </summary>
    public abstract class AbstractLevelAct
    {
        public AbstractLevelAct nextLevelAct;

        public abstract void Play();
    }
}

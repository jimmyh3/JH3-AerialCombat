﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUnitCache : MonoBehaviour {

    /// <summary>Tracks <see cref="GameUnit"/>s via their <see cref="Object.GetInstanceID"/>.</summary>
    private static Dictionary<int, GameUnit> gameUnitCache = new Dictionary<int, GameUnit>();

    public static void Add(GameUnit gameUnit)
    {
        gameUnitCache[gameUnit.GetInstanceID()] = gameUnit;
    }

    public static void Remove(GameUnit gameUnit)
    {
        int instanceID = gameUnit.GetInstanceID();

        if (gameUnitCache.TryGetValue(instanceID, out gameUnit))
            gameUnitCache.Remove(instanceID);
    }

    public static void Add(List<GameUnit> gameUnits)
    {
        foreach (GameUnit gameUnit in gameUnits)
            Add(gameUnit);
    }

    public static void Remove(List<GameUnit> gameUnits)
    {
        foreach (GameUnit gameUnit in gameUnits)
            Remove(gameUnit);
    }
}

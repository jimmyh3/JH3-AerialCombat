﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[DisallowMultipleComponent]
public abstract class GameUnit : MonoBehaviour {

    // Variables
    [Tooltip("Place colliders that make up the body of this unit here.")]
    public List<Collider> bodyColliders = new List<Collider>();
    [Tooltip("Values defining the identification and allegiances of this unit.")]
    public UnitCompFriendFoe idFriendFoeComponent = new UnitCompFriendFoe();
    

    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    public virtual void OnValidate()
    {
        if (bodyColliders.Count == 0)
            Debug.LogWarning(this.ToString() + ": There are no body Colliders listed!");
    }
    
    // ***********************
    //
    //      Helper Classes
    //
    // ***********************


    /// <summary>
    /// <see cref="TrackedUnits"/> is a class used to track <see cref="GameUnit"/>s
    /// that are optionally added here for quick group referencing. This class DOES NOT indicate whether
    /// a <see cref="GameUnit"/> belongs to any particular <see cref="UnitCompFriendFoe.faction"/>.
    /// </summary>
    public static class TrackedUnits
    {

        /// <summary>
        /// <see cref="Dictionary{TKey, TValue}"/> of <see cref="GameUnit"/> Lists
        /// that contain units corresponding to each faction
        /// </summary>
        private static Dictionary<GameUnitFriendFoeManager.Faction, Dictionary<int, GameUnit>> factionToUnitsDictionary = new Dictionary<GameUnitFriendFoeManager.Faction, Dictionary<int, GameUnit>>();


        // ************************
        //
        //      Static Methods
        //
        // ************************

        /// <summary>Static initializer method.</summary>
        static TrackedUnits()
        {
            foreach (GameUnitFriendFoeManager.Faction faction in System.Enum.GetValues(typeof(GameUnitFriendFoeManager.Faction)))
            {
                factionToUnitsDictionary.Add(faction, new Dictionary<int, GameUnit>());
            }

        }

        /// <summary>
        /// <para>
        /// Add the given <see cref="GameUnit"/> to a tracking <see cref="Dictionary{TKey, TValue}"/>. Its tracking
        /// <see cref="Dictionary{TKey, TValue}"/> is identified by the <see cref="GameUnit"/>'s <see cref="UnitCompFriendFoe.faction"/>;
        /// see <see cref="GetTrackedByFaction(GameUnitFriendFoeManager.Faction)"/> for more details.
        /// </para>
        /// 
        /// Note that if the respective <see cref="Dictionary{TKey, TValue}"/> of the given <see cref="GameUnit"/>'s <see cref="UnitCompFriendFoe.faction"/>
        /// cannot be found, then it will be made, and the <see cref="GameUnit"/> will then be added to it. The <see cref="Object.GetInstanceID"/> is used to 
        /// uniquely identify the <see cref="GameUnit"/> so that duplicates will not be added.
        /// </summary>
        /// 
        /// <param name="gameUnit">The <see cref="GameUnit"/> to add.</param>
        public static void AddTrackedByFaction(GameUnit gameUnit)
        {
            if (gameUnit == null) return;
            
            Dictionary<int, GameUnit> unitsDictionary = null;   // Stores units of a particular Faction.

            // If Dictionary that lists units belonging to a Faction does not exists then create it and link it.
            if (!factionToUnitsDictionary.TryGetValue(gameUnit.idFriendFoeComponent.faction, out unitsDictionary))
            {
                unitsDictionary = new Dictionary<int, GameUnit>();
                factionToUnitsDictionary.Add(gameUnit.idFriendFoeComponent.faction, unitsDictionary);
            }

            // Add the GameUnit to the unit dictionary using its ID as a key.
            unitsDictionary[gameUnit.GetInstanceID()] = gameUnit;
        }
        
        /// <summary>
        /// Removes the given <see cref="GameUnit"/>. It is <see cref="GameUnit.idFriendFoeComponent"/> to
        /// identify the <see cref="UnitCompFriendFoe.faction"/> it belongs to and attempts to remove it
        /// if it exists.
        /// </summary>
        /// <param name="gameUnit">The <see cref="GameUnit"/> to remove from tracking by <see cref="GameUnitFriendFoeManager.Faction"/></param>
        public static void RemoveTrackedByFaction(GameUnit gameUnit)
        {
            if (gameUnit == null) return;

            Dictionary<int, GameUnit> unitsDictionary = null; // Stores units of a particular Faction.

            if (factionToUnitsDictionary.TryGetValue(gameUnit.idFriendFoeComponent.faction, out unitsDictionary))
                if (unitsDictionary.ContainsKey(gameUnit.GetInstanceID()))
                    unitsDictionary.Remove(gameUnit.GetInstanceID());
            
        }

        /// <summary>
        /// Returns all <see cref="GameUnit"/> that have been tracked via <see cref="AddTrackedByFaction(GameUnit)"/>
        /// belonging to a specific <see cref="UnitCompFriendFoe.faction"/> as a <see cref="List{T}"/>.
        /// </summary>
        /// 
        /// <param name="faction">
        /// The key to retrieve the <see cref="GameUnit"/>s belonging to a specific <see cref="UnitCompFriendFoe.faction"/>
        /// </param>
        /// 
        /// <returns><see cref="List{T}"/> of GameUnits belongning to a specific <see cref="UnitCompFriendFoe.faction"/>.</returns>
        public static List<GameUnit> GetTrackedByFaction(GameUnitFriendFoeManager.Faction faction)
        {
            Dictionary<int, GameUnit> unitsDictionary = null;

            if (factionToUnitsDictionary.TryGetValue(faction, out unitsDictionary))
            {
                RemoveNullUnits(unitsDictionary);
                return unitsDictionary.Values.ToList();
            }

            return null;
        }

        /// <summary>
        /// Get ALL <see cref="GameUnit"/>s that are opposed to the given <see cref="GameUnitFriendFoeManager.Faction"/>.
        /// </summary>
        /// <param name="myFaction">The <see cref="GameUnitFriendFoeManager.Faction"/> to find opposition to.</param>
        /// <returns><see cref="List{T}"/> of opposing <see cref="GameUnit"/>s.</returns>
        public static List<GameUnit> GetTrackedByFactionOpposition(GameUnitFriendFoeManager.Faction myFaction)
        {
            List<GameUnit> opposingUnits;                   // List of all opposing untis to return.
            GameUnitFriendFoeManager.Faction opposingFactions;    // Opposing allegiance of the given faction.

            opposingUnits = new List<GameUnit>();
            opposingFactions = ~(GameUnitFriendFoeManager.Instance.factionAllegianceMap[myFaction]);
            
            foreach (GameUnitFriendFoeManager.Faction opposingFaction in System.Enum.GetValues(typeof(GameUnitFriendFoeManager.Faction)))
            {
                // Bitwise check if this current faction is an opposing faction.
                if ((opposingFactions & opposingFaction) == opposingFaction)
                {
                    Dictionary<int, GameUnit> opposingUnitsDictionary = null;
                    if (factionToUnitsDictionary.TryGetValue(opposingFaction, out opposingUnitsDictionary))
                    {
                        RemoveNullUnits(opposingUnitsDictionary);
                        opposingUnits.AddRange(opposingUnitsDictionary.Values.ToList());
                    }
                }
            }
            return opposingUnits;
        }

        /// <summary>
        /// Helper method to remove null <see cref="GameUnit"/> values from a <see cref="Dictionary{TKey, TValue}"/>.
        /// </summary>
        /// <param name="unitsDictionary"><see cref="Dictionary{TKey, TValue}"/> to remove null values from.</param>
        private static void RemoveNullUnits(Dictionary<int, GameUnit> unitsDictionary)
        {
            foreach (int key in unitsDictionary.Keys.ToList())
            {
                GameUnit value = null;
                if (unitsDictionary.TryGetValue(key, out value))
                    if (value == null)
                        unitsDictionary.Remove(key);
            }
        }

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(UnitCompDamageCollision))]
public abstract class Projectile : GameUnit {

    // ***** Public/Inspector Fields *****
    //[Tooltip("Values defining rate of lateral movement.")]
    //public MoveSpeedComponent moveSpeedComponent = new MoveSpeedComponent();
    //[Tooltip("Values defining the damage dealt upon impacting an opposing unit.")]
    //public DamageCollisionComponent damageCollisionComponent = new DamageCollisionComponent();
    [SerializeField]
    [Tooltip("Audio clips to be used by this Projectile.")]
    private ProjectileAudio projectileAudio;
    // ***** Private Fields *****
    private GameUnit _projectileOwner;
    private Rigidbody _rigidbody;
    private UnitCompMoveSpeed _moveSpeedComponent;
    private UnitCompDamageCollision _damageCollisionComponent;

    // ***** Properties *****
    public UnitCompMoveSpeed moveSpeedComponent
    {
        get
        {
            if (_moveSpeedComponent == null)
                _moveSpeedComponent = GetComponent<UnitCompMoveSpeed>();

            return _moveSpeedComponent;
        }
    }

    public UnitCompDamageCollision damageCollisionComponent
    {
        get
        {
            if (_damageCollisionComponent == null)
                _damageCollisionComponent = GetComponent<UnitCompDamageCollision>();

            return _damageCollisionComponent;
        }
    }

    public new Rigidbody rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();
            return _rigidbody;
        }
    }
    
    /// <summary>The shooter of this <see cref="Projectile"/>.</summary>
    public GameUnit projectileOwner
    {
        get { return _projectileOwner; }
        set
        {
            _projectileOwner = value;

            if (_projectileOwner == null) return;
            if (this.idFriendFoeComponent != null && _projectileOwner.idFriendFoeComponent != null)
                this.idFriendFoeComponent.faction = _projectileOwner.idFriendFoeComponent.faction;
        }
    }

    // **********************
    //
    //      Unity Methods
    // 
    // **********************

    public override void OnValidate()
    {
        base.OnValidate();

        if (moveSpeedComponent != null)
            moveSpeedComponent.OnValidate();

        if (damageCollisionComponent != null)
            damageCollisionComponent.OnValidate();
        
        if (rigidbody == null)
        {
            gameObject.AddComponent<Rigidbody>();
            Debug.LogWarning(this.ToString() + ": Rigidbody script was missing but has been added!", this);
        }

        if (rigidbody != null && rigidbody.useGravity)
        {
            rigidbody.useGravity = false;
            Debug.LogWarning(this.ToString() + ": Rigidbody.useGravity has been set to FALSE to ensure proper movement.", this);
        }

        if (rigidbody != null && !rigidbody.isKinematic)
        {
            rigidbody.isKinematic = true;
            Debug.LogWarning(this.ToString() + ": Rigidbody.isKinematic has been set to TRUE to ensure proper movement", this);
        }

        if ((rigidbody.constraints & (RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY))
                                   != ((RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY)))
        {
            rigidbody.constraints |= RigidbodyConstraints.FreezeRotationX;
            rigidbody.constraints |= RigidbodyConstraints.FreezePositionY;
            Debug.LogWarning(this.ToString() + ": RigidbodyConstraints set to constain FreezeRotationX and FreezePositionY.");
        }
    }
    
    // **********************
    //
    //      Helper Classes
    // 
    // **********************

    [System.Serializable]
    private class ProjectileAudio
    {
        [Tooltip("Audio clip of this projectile's collision.")]
        public AudioClip impactClip;

        public AudioSource impactAS { get; set; }
    }
}

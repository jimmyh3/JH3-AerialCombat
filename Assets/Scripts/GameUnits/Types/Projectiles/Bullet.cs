﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider))]
public class Bullet : Projectile {

    // *******************************
    //
    //          Unity Methods
    //
    // *******************************

    public override void OnValidate()
    {
        base.OnValidate();

        CapsuleCollider capsuleCollider = GetComponent<CapsuleCollider>();

        if (!capsuleCollider.isTrigger)
        {
            capsuleCollider.isTrigger = true;
            Debug.LogWarning(this.ToString() + ": SphereCollider isTrigger has been set true as required.", this);
        }
    }

    protected void Start()
    {
        OnValidate();
    }

    private void OnTriggerEnter(Collider other)
    {
        GameUnit otherUnit = other.GetComponentInParent<GameUnit>();

        if (otherUnit == projectileOwner) return;

        if (GameUnitFriendFoeManager.Instance.IsEnemy(this, otherUnit))
        {
            UnitCompHealth otherHealthComp = other.GetComponentInParent<UnitCompHealth>();

            if (otherHealthComp != null)
                otherHealthComp.ReceiveDamage(damageCollisionComponent.damage);
            else
                Debug.LogWarning(other.ToString() + ": HealthComponent cannot be found!");

            if (damageCollisionComponent != null && damageCollisionComponent.onCollisionEffect != null)
                Instantiate(damageCollisionComponent.onCollisionEffect, transform.position, transform.rotation);

            Destroy(gameObject);
        }
    }

    public void Update()
    {
        transform.Translate(transform.forward * moveSpeedComponent.maxSpeed * Time.deltaTime, Space.World);
    }
}

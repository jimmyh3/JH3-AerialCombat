﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitCompMoveSpeed))]
[RequireComponent(typeof(UnitCompHealth))]
[RequireComponent(typeof(Rigidbody))]
public class Aircraft : CombatUnit
{

    // *********************
    //
    //      Variables
    //
    // *********************

    // ***** Public/Inspector Fields *****
    [Tooltip("Attach Weapon GameObjects here for usage.")]
    public List<Weapon> weaponList = new List<Weapon>();
    //[Tooltip("Values defining rate of lateral movement.")]
    //public MoveSpeedComponent moveSpeedComponent = new MoveSpeedComponent();
    //[Tooltip("Values defining rate of rotation.")]
    //public RotateSpeedComponent rotateSpeedComponent = new RotateSpeedComponent();
    //[Tooltip("Values defining the tilting/banking of this object when turning.")]
    //public MoveTiltComponent moveTiltComponent = new MoveTiltComponent();
    //[Tooltip("Values defining the vision range of this object.")]
    //public SightRangeComponent sightRangeComponent = new SightRangeComponent();
    [SerializeField]
    [Tooltip("Audio clips to be used by this Aircraft.")]
    private AircraftAudio aircraftAudio;

    // ***** Private Fields *****
    private UnitCompMoveSpeed _moveSpeedComponent;
    private UnitCompRotateSpeed _rotateSpeedComponent;
    private UnitCompMoveTilt _moveTiltComponent;
    private UnitCompSightRange _sightRangeComponent;
    private UnitCompHealth _healthComponent;
    private Rigidbody _rigidbody;

    // *********************
    //
    //      Properties
    //
    // *********************
    public UnitCompMoveSpeed moveSpeedComponent
    {
        get
        {
            if (_moveSpeedComponent == null)
                _moveSpeedComponent = GetComponent<UnitCompMoveSpeed>();

            return _moveSpeedComponent;
        }
    }

    public UnitCompRotateSpeed rotateSpeedComponent
    {
        get
        {
            if (_rotateSpeedComponent == null)
                _rotateSpeedComponent = GetComponent<UnitCompRotateSpeed>();

            return _rotateSpeedComponent;
        }
    }

    public UnitCompMoveTilt moveTiltComponent
    {
        get
        {
            if (_moveTiltComponent == null)
                _moveTiltComponent = GetComponent<UnitCompMoveTilt>();

            return _moveTiltComponent;
        }
    }

    public UnitCompSightRange sightRangeComponent
    {
        get
        {
            if (_sightRangeComponent == null)
                _sightRangeComponent = GetComponent<UnitCompSightRange>();

            return _sightRangeComponent;
        }
    }

    public UnitCompHealth healthComponent
    {
        get
        {
            if (_healthComponent == null)
                _healthComponent = GetComponent<UnitCompHealth>();

            if (_healthComponent == null)
            {
                Debug.LogWarning(this.ToString() + ": HealthComponent was missing but has been added!", this);
                _healthComponent = gameObject.AddComponent<UnitCompHealth>();
            }

            return _healthComponent;
        }
    }

    public new Rigidbody rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();

            if (_rigidbody == null)
            {
                Debug.LogWarning(this.ToString() + ": Rigidbody script was missing but has been added!", this);
                _rigidbody = gameObject.AddComponent<Rigidbody>();
            }

            return _rigidbody;
        }
    }


    // *********************
    //
    //      Unity Methods
    //
    // *********************

    public override void OnValidate()
    {
        base.OnValidate();

        //moveSpeedComponent.OnValidate();
        //rotateSpeedComponent.OnValidate();
        //moveTiltComponent.OnValidate();
        //sightRangeComponent.OnValidate();

        if (healthComponent == null)
        {
            _healthComponent = gameObject.AddComponent<UnitCompHealth>();
            Debug.LogWarning(this.ToString() + ": HealthComponent was missing but has been added!", this);
        }

        if (rigidbody == null)
        {
            gameObject.AddComponent<Rigidbody>();
            Debug.LogWarning(this.ToString() + ": Rigidbody script was missing but has been added!", this);
        }

        if (rigidbody != null && rigidbody.useGravity)
        {
            rigidbody.useGravity = false;
            Debug.LogWarning(this.ToString() + ": Rigidbody.useGravity has been set to FALSE to ensure proper movement.", this);
        }

        //if (rigidbody != null && rigidbody.isKinematic)
        //{
        //    rigidbody.isKinematic = false;
        //    Debug.LogWarning(this.ToString() + ": Rigidbody.isKinematic has been set to FALSE to ensure proper collision.", this);
        //}

        if ((rigidbody.constraints & (RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY))
                                   != ((RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY)))
        {
            rigidbody.constraints |= RigidbodyConstraints.FreezeRotationX;
            rigidbody.constraints |= RigidbodyConstraints.FreezePositionY;
            Debug.LogWarning(this.ToString() + ": RigidbodyConstraints set to constain FreezeRotationX and FreezePositionY.");
        }

        /*
         * Removes missing Weapon script references and 
         * add new unlisted Weapon script references that are
         * children of this GameObject.
         */
        List<int> missingWeaponList = new List<int>();
        for (int i = 0; i < weaponList.Count; i++)
            if (weaponList[i] == null)
                missingWeaponList.Add(i);

        foreach (int x in missingWeaponList)
            weaponList.RemoveAt(x);

        foreach (Weapon x in GetComponentsInChildren<Weapon>())
            if (!weaponList.Contains(x))
                weaponList.Add(x);
    }

    protected override void Start()
    {
        base.Start();
        OnValidate();

        aircraftAudio.engineAS = gameObject.AddComponent<AudioSource>();
        aircraftAudio.engineAS.clip = aircraftAudio.engineClip;
        aircraftAudio.engineAS.loop = true;
        aircraftAudio.engineAS.Play();

        aircraftAudio.boostingAS = gameObject.AddComponent<AudioSource>();
        aircraftAudio.boostingAS.clip = aircraftAudio.boostingClip;
        aircraftAudio.boostingAS.loop = false;
    }

    private void OnDrawGizmosSelected()
    {
        Vector3 center;
        Vector3 size;

        center = transform.position + (transform.forward * sightRangeComponent.maxDistance / 2.0f);
        size = new Vector3(sightRangeComponent.halfWidthX * 2.0f,
                                sightRangeComponent.halfHeightY * 2.0f,
                                    sightRangeComponent.maxDistance);

        Matrix4x4 oldMatrix = Gizmos.matrix;
        Color oldColor = Gizmos.color;

        Gizmos.matrix.SetTRS(Vector3.zero, transform.rotation, transform.localScale);
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireCube(center, size);
        Gizmos.matrix = oldMatrix;
        Gizmos.color = oldColor;
    }
    
    [System.Serializable]
    private class AircraftAudio
    {
        [Tooltip("Audio clip of a running engine.")]
        public AudioClip engineClip;
        [Tooltip("Audio clip of boosting thrusters")]
        public AudioClip boostingClip;

        public AudioSource engineAS { get; set; }
        public AudioSource boostingAS { get; set; }
        
    }
}

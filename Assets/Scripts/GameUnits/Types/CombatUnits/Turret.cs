﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : CombatUnit {

    // *********************
    //
    //      Variables
    //
    // *********************

    /// <summary> The relative horizontal turning point of the turret. </summary>
    [Tooltip("The relative horizontal turning point of the turret.")]
    public GameObject turretHead;
    /// <summary> The relative vertical turning point of the turret. </summary>
    [Tooltip("The relative vertical turning point of the turret.")]
    public GameObject turretFace;
    [Tooltip("Attach Weapon GameObjects here for usage.")]
    public List<Weapon> weaponList = new List<Weapon>();
    //[Tooltip("Values defining rate of rotation.")]
    //public RotateSpeedComponent rotateSpeedComponent = new RotateSpeedComponent();
    //[Tooltip("Values defining spherical detection range.")]
    //public DetectionRangeComponent detectionRangeComponent = new DetectionRangeComponent();

    private UnitCompRotateSpeed _rotateSpeedComponent;
    private UnitCompDetectionRange _detectionRangeComponent;
    private UnitCompHealth _healthComponent;
    private Rigidbody _rigidbody;

    // *********************
    //
    //      Properties
    //
    // *********************

    public UnitCompRotateSpeed rotateSpeedComponent
    {
        get
        {
            if (_rotateSpeedComponent == null)
                _rotateSpeedComponent = GetComponent<UnitCompRotateSpeed>();

            return _rotateSpeedComponent;
        }
    }

    public UnitCompDetectionRange detectionRangeComponent
    {
        get
        {
            if (_detectionRangeComponent == null)
                _detectionRangeComponent = GetComponent<UnitCompDetectionRange>();

            return _detectionRangeComponent;
        }
    }

    public UnitCompHealth healthComponent
    {
        get
        {
            if (_healthComponent == null)
                _healthComponent = GetComponent<UnitCompHealth>();

            if (_healthComponent == null)
            {
                Debug.LogWarning(this.ToString() + ": HealthComponent was missing but has been added!", this);
                _healthComponent = gameObject.AddComponent<UnitCompHealth>();
            }

            return _healthComponent;
        }
    }

    public new Rigidbody rigidbody
    {
        get
        {
            if (_rigidbody == null)
                _rigidbody = GetComponent<Rigidbody>();

            if (_rigidbody == null)
            {
                Debug.LogWarning(this.ToString() + ": Rigidbody script was missing but has been added!", this);
                _rigidbody = gameObject.AddComponent<Rigidbody>();
            }

            return _rigidbody;
        }
    }


    // *********************
    //
    //      Unity Methods
    //
    // *********************

    public override void OnValidate()
    {
        base.OnValidate();

        //rotateSpeedComponent.OnValidate();

        if (turretHead == null)
        {
            Debug.LogError(this.GetType().Name + ": turretHead property is required!");
        }

        if (turretFace == null)
        {
            Debug.LogError(this.GetType().Name + ": turretHead property is required!");
        }

        if (healthComponent == null)
        {
            _healthComponent = gameObject.AddComponent<UnitCompHealth>();
            Debug.LogWarning(this.GetType().Name + ": HealthComponent was missing but has been added!", this);
        }

        if (rigidbody == null)
        {
            gameObject.AddComponent<Rigidbody>();
            Debug.LogWarning(this.GetType().Name + ": Rigidbody script was missing but has been added!", this);
        }

        if (rigidbody != null && rigidbody.useGravity)
        {
            rigidbody.useGravity = false;
            Debug.LogWarning(this.GetType().Name + ": Rigidbody.useGravity has been set to FALSE to ensure proper movement.", this);
        }

        //if (rigidbody != null && rigidbody.isKinematic)
        //{
        //    rigidbody.isKinematic = false;
        //    Debug.LogWarning(this.ToString() + ": Rigidbody.isKinematic has been set to FALSE to ensure proper collision.", this);
        //}

        if ((rigidbody.constraints & (RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY))
                                   != ((RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezePositionY)))
        {
            rigidbody.constraints |= RigidbodyConstraints.FreezeRotationX;
            rigidbody.constraints |= RigidbodyConstraints.FreezePositionY;
            Debug.LogWarning(this.GetType().Name + ": RigidbodyConstraints set to constain FreezeRotationX and FreezePositionY.");
        }

        /*
         * Removes missing Weapon script references and 
         * add new unlisted Weapon script references that are
         * children of this GameObject.
         */
        List<int> missingWeaponList = new List<int>();
        for (int i = 0; i < weaponList.Count; i++)
            if (weaponList[i] == null)
                missingWeaponList.Add(i);

        foreach (int x in missingWeaponList)
            weaponList.RemoveAt(x);

        foreach (Weapon x in GetComponentsInChildren<Weapon>())
            if (!weaponList.Contains(x))
                weaponList.Add(x);
    }

    protected override void Start()
    {
        base.Start();
        OnValidate();
    }

    private void OnDrawGizmosSelected()
    {
        Color oldColor = Gizmos.color;
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, detectionRangeComponent.detectionRadius);
        Gizmos.color = oldColor;
    }
}

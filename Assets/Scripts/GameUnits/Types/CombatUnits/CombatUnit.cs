﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatUnit : GameUnit {

    protected virtual void Start()
    {
        TrackedUnits.AddTrackedByFaction(this);
    }

    protected virtual void OnDestroy()
    {
        TrackedUnits.RemoveTrackedByFaction(this);
    }

}

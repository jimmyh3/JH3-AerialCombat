﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This <see cref="GameUnitFriendFoeManager"/> checks if two <see cref="GameUnit"/> with <see cref="UnitCompFriendFoe"/>
/// are in opposition of each other based on their <see cref="Faction"/> and <see cref="FactionAllegiance"/>. This
/// object should exist in the current scene to allow setting Faction allegiances in edit time.
/// </summary>
[DisallowMultipleComponent]
public class GameUnitFriendFoeManager : MonoBehaviour {



    // ********************************
    //
    //      Enums & Inner Classes
    //
    // ********************************

    [System.Flags]
    public enum Faction
    {
        // Note:
        //  Additional Factions should be added to
        //  FactionAllegiance respectively as well.
        Neutral = 1,
        Aarcodian = 1 << 1,
        Playora = 1 << 2
    }

    [System.Serializable]
    public class FactionAllegiance
    {
        [EnumFlag] [SerializeField] private Faction _aarcodianAllies = Faction.Aarcodian;
        [EnumFlag] [SerializeField] private Faction _playoraAllies = Faction.Playora;
        
        public Faction aarcodianAllies
        {
            get { return _aarcodianAllies; }
            set { _aarcodianAllies = value | Faction.Aarcodian; }
        }
        
        public Faction playoraAllies
        {
            get { return _playoraAllies; }
            set { _playoraAllies = value | Faction.Playora; }
        }
    }



    // ********************************
    //
    //      Variables & Properties
    //
    // ********************************
    
    // Variables
    public Dictionary<Faction, Faction> factionAllegianceMap = new Dictionary<Faction, Faction>();
    [SerializeField] private FactionAllegiance factionAllegiance;
    private static GameUnitFriendFoeManager _instance;

    // Properties
    public static GameUnitFriendFoeManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType(typeof(GameUnitFriendFoeManager)) as GameUnitFriendFoeManager;

            if (_instance == null)
            {
                GameObject gobj = new GameObject("IDFriendFoeManager");
                _instance = gobj.AddComponent(typeof(GameUnitFriendFoeManager)) as GameUnitFriendFoeManager;

                Debug.Log("IDFriendFoeManager: Could not locate GameObject with IDFriendFoeManager. "
                                                + "A IDFriendFoeManager has been made automatically.");
            }

            return _instance;
        }
    }

    // ********************************
    //
    //          Unity Methods
    //
    // ********************************

    private void OnApplicationQuit()
    {
        _instance = null;
    }
    
    private void Awake()
    {
        // Ensure Singleton on scene.
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
        
    }

    private void Start()
    {
        OnValidate();

        factionAllegianceMap[Faction.Neutral] = Faction.Neutral;
        factionAllegianceMap[Faction.Aarcodian] = factionAllegiance.aarcodianAllies;
        factionAllegianceMap[Faction.Playora] = factionAllegiance.playoraAllies;
    }

    private void OnValidate()
    {
        // Warn with error if multiple instances of Player Manager was found on scene.
        if (FindObjectsOfType<GameUnitFriendFoeManager>().Length > 1)
            Debug.LogError(typeof(GameUnitFriendFoeManager).Name + ": requires only one instance of itself on the scene!", this);

        factionAllegiance.aarcodianAllies = factionAllegiance.aarcodianAllies;
        factionAllegiance.playoraAllies = factionAllegiance.playoraAllies;
    }

    // ********************************
    //
    //          Class Methods
    //
    // ********************************

    /// <summary> Private constructor to disallow instantiation. </summary>
    private GameUnitFriendFoeManager() { }
    

    public bool IsEnemy(GameUnit me, GameUnit badguy)
    {
        if (me == null || badguy == null) return false;
        
        UnitCompFriendFoe meIFFComp = me.idFriendFoeComponent;
        UnitCompFriendFoe badguyIFFComp = badguy.idFriendFoeComponent;

        return IsEnemy(meIFFComp, badguyIFFComp);
    }

    public bool IsEnemy(UnitCompFriendFoe myIFFComp, UnitCompFriendFoe badguyIFFComp)
    {
        if (myIFFComp == null || badguyIFFComp == null) return false;
        return IsEnemy(myIFFComp.faction, badguyIFFComp.faction);
    }

    public bool IsEnemy(Faction myFaction, Faction badguyFaction)
    {
        Faction factionAllegiance = factionAllegianceMap[myFaction];
        return ((factionAllegiance & badguyFaction) != badguyFaction);
    }
    
}


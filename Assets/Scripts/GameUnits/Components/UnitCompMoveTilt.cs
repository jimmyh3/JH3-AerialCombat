﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This class holds values on how much the ship should bank when moving
/// on certain axes.
/// </summary>
[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompMoveTilt : AbstractComponentJH3 {

    // Variables
    [SerializeField]
    [Tooltip("Max rate of tilt along the X-axis rotation")]
    private float _maxRateOfTiltX = 0.0f;
    [SerializeField]
    [Tooltip("Max rate of tilt along the Y-axis rotation")]
    private float _maxRateOfTiltY = 0.0f;
    [SerializeField]
    [Tooltip("Max rate of tilt along the Z-axis rotation")]
    private float _maxRateOfTiltZ = 20.0f;
    [SerializeField]
    [Tooltip("Max angle of tilt along the X-axis rotation")]
    private float _maxTiltAngleX = 0.0f;
    [SerializeField]
    [Tooltip("Max angle of tilt along the X-axis rotation")]
    private float _maxTiltAngleY = 0.0f;
    [SerializeField]
    [Tooltip("Max angle of tilt along the X-axis rotation")]
    private float _maxTiltAngleZ = 22.5f;

    // Properties
    /// <summary>Max rate of tilt along the X-axis rotation</summary>
    public float maxRateOfTiltX
    {
        get { return _maxRateOfTiltX; }
        set { _maxRateOfTiltX = Mathf.Abs(value); }
    }

    /// <summary>Max rate of tilt along the Y-axis rotation</summary>
    public float maxRateOfTiltY
    {
        get { return _maxRateOfTiltY; }
        set { _maxRateOfTiltY = Mathf.Abs(value); }
    }

    /// <summary>Max rate of tilt along the Z-axis rotation</summary>
    public float maxRateOfTiltZ
    {
        get { return _maxRateOfTiltZ; }
        set { _maxRateOfTiltZ = Mathf.Abs(value); }
    }

    /// <summary>Max angle of tilt along the X-axis rotation</summary>
    public float maxTiltAngleX
    {
        get { return _maxTiltAngleX; }
        set { _maxTiltAngleX = Mathf.Abs(value); }
    }

    /// <summary>Max angle of tilt along the Y-axis rotation</summary>
    public float maxTiltAngleY
    {
        get { return _maxTiltAngleY; }
        set { _maxTiltAngleY = Mathf.Abs(value); }
    }

    /// <summary>Max angle of tilt along the Z-axis rotation</summary>
    public float maxTiltAngleZ
    {
        get { return _maxTiltAngleZ; }
        set { _maxTiltAngleZ = Mathf.Abs(value); }
    }


    // Unity Methods
    public void OnValidate()
    {
        maxRateOfTiltX = maxRateOfTiltX;
        maxRateOfTiltY = maxRateOfTiltY;
        maxRateOfTiltZ = maxRateOfTiltZ;
        maxTiltAngleX = maxTiltAngleX;
        maxTiltAngleY = maxTiltAngleY;
        maxTiltAngleZ = maxTiltAngleZ;
    }

}

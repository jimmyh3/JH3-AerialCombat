﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class AbstractComponentJH3 : MonoBehaviour {

	protected virtual void OnValidate()
    {
        // Removes duplicates of the same script on the same GameObject.
        Component[] parents = GetComponents(GetType());
        //From https://forum.unity3d.com/threads/onvalidate-and-destroying-objects.258782/
        if (parents.Length > 1 && parents[0] != this)
        {
            UnityEditor.EditorApplication.delayCall += () =>
            {
                Debug.LogWarning(this.GetType().Name + " script: has been REMOVED! A prior " 
                                                     + this.GetType().Name + " script exists.");
                DestroyImmediate(this);
            };
        }
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class UnitCompDeathZeroHP : AbstractComponentJH3
{
    [Tooltip("Determines if this GameObject shall be destroyed upon death or just disabled.")]
    public bool isDestroyable = true;
    [Tooltip("The effect to instantiate upon death.")]
    public GameObject instantiateOnDeath;

}

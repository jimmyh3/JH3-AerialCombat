﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <see cref="UnitCompSightRange"/> contains primitive values used for casting a
/// <see cref="Physics.BoxCastAll(Vector3, Vector3, Vector3, Quaternion, float, int)"/>.
/// An example use would be for <see cref="GameUnit"/> to identify other <see cref="GameUnit"/>
/// that a certain distance away.
/// </summary>
[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompSightRange : AbstractComponentJH3 {

    // ***********************
    //
    //      Variables
    //
    // ***********************

    [SerializeField]
    [Tooltip("Range distance along the unit's X-axis. "
                + "Note that this value is applied to the unit's left and right (thus called 'half').")]
    private float _halfWidthX  = 5;   // x

    [SerializeField]
    [Tooltip("Range distance along the unit's Y-axis. "
                + "Note that this value is applied to the unit's up and down (thus called 'half').")]
    private float _halfHeightY = 5;   // y

    [SerializeField]
    [Tooltip("Range distance along the unit's Z-axis.")]
    private float _maxDistance = 10;


    // ***********************
    //
    //      Properties
    //
    // ***********************

    /// <summary>
    /// Range distance along the unit's X-axis.
    /// Note that this value is applied up and down (thus called 'half').
    /// </summary>
    public float halfWidthX
    {
        get { return _halfWidthX; }
        set { _halfWidthX = Mathf.Abs(value); }
    }

    /// <summary>
    /// Range distance along the unit's Y-axis.
    /// Note that this value is applied up and down (thus called 'half').
    /// </summary>
    public float halfHeightY
    {
        get { return _halfHeightY; }
        set { _halfHeightY = Mathf.Abs(value); }
    }

    /// <summary>Range distance along the unit's Z-axis.</summary>
    public float maxDistance
    {
        get { return _maxDistance; }
        set { _maxDistance = Mathf.Abs(value); }
    }


    // Unity Methods
    public void OnValidate()
    {
        halfWidthX = halfWidthX;
        halfHeightY = halfHeightY;
        maxDistance = maxDistance;
    }

    //private void OnDrawGizmosSelected()
    //{
    //    Vector3 center;
    //    Vector3 size;

    //    center = transform.position + (transform.forward * maxDistance / 2.0f);
    //    size = new Vector3(halfWidthX * 2.0f, halfHeightY * 2.0f, maxDistance);

    //    Gizmos.color = Color.cyan;
    //    Gizmos.DrawWireCube(center, size);
    //}
}

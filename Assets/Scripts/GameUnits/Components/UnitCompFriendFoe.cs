﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitCompFriendFoe {
    
    // Variables
    //[SerializeField]
    //private string _name;
    
    [Tooltip("Used to determine friend or foe against other units.")]
    public GameUnitFriendFoeManager.Faction faction = GameUnitFriendFoeManager.Faction.Neutral;
    

    //// Properties
    //public string name
    //{
    //    get { return _name; }
    //    private set { _name = value; }
    //}
    
}

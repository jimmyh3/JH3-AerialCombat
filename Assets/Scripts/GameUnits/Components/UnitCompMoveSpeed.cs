﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompMoveSpeed : AbstractComponentJH3 {

    // Variables 
    private float _currentSpeed;
    private float _isAccelerating = 0.0f;
    //private float _currentAcceleration = 0.0f;
    //private float _currentDeceleration = 0.0f;

    [SerializeField]
    [Tooltip("The maximum acceleration of this unit. The value is set to positive.")]
    private float _maxAcceleration = 2.0f;
    [SerializeField]
    [Tooltip("The maximum deceleration of this unit. The value is set to positive.")]
    private float _maxDeceleration = 5.0f;
    [SerializeField]
    [Tooltip("The maximum speed of this unit. The value is set to positive.")]
    private float _maxSpeed = 10.0f;
    
    // Properties
    /// <summary>
    /// Positive value indicates that it is accelerating.
    /// Negative value indicates that it is decelerating.
    /// Zero value indicates that is neither accelerating nor decelerating.
    /// </summary>
    public float isAccelerating
    {
        get { return _isAccelerating; }
        set { _isAccelerating = value; }
    }
    
    /// <summary>The maximum acceleration of this unit. The value is absolute.</summary>
    public float maxAcceleration
    {
        get { return _maxAcceleration; }
        set { _maxAcceleration = Mathf.Abs(value); }
    }

    /// <summary>The maximum deceleration of this unit. The value is absolute.</summary>
    public float maxDeceleration
    {
        get { return _maxDeceleration; }
        set { _maxDeceleration = Mathf.Abs(value); }
    }

    /// <summary>The maximum speed of this unit. The value is absolute.</summary>
    public float maxSpeed
    {
        get { return _maxSpeed; }
        set { _maxSpeed = Mathf.Abs(value); }
    }

    /// <summary>The current speed of this unit. The value is absolute.</summary>
    public float currentSpeed
    {
        get { return _currentSpeed; }
        set { _currentSpeed = Mathf.Clamp(value, 0.0f, maxSpeed); }
    }


    // Unity Methods
    public void OnValidate()
    {
        maxAcceleration = maxAcceleration;
        maxDeceleration = maxDeceleration;
        maxSpeed = maxSpeed;
    }

}

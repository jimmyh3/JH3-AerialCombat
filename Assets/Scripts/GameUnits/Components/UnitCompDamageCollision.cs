﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompDamageCollision : AbstractComponentJH3 {

    [Tooltip("Effect to instantiate upon collision on a object.")]
    public GameObject onCollisionEffect;

    [SerializeField]
    [Tooltip("The damage dealt upon colliding with another unit. "
                + " The value is clamped between ZERO and int.MaxValue.")]
    private int _damage = 10;

    /// <summary>
    /// The damage dealt upon colliding with another <see cref="GameUnit"/>.
    /// The damage value is clamped between ZERO and <see cref="int.MaxValue"/>.
    /// </summary>
    public int damage
    {
        get { return _damage; }
        set { _damage = Mathf.Clamp(value, 0, int.MaxValue); }
    }


    public new void OnValidate()
    {
        base.OnValidate();
        damage = damage;
    }
}

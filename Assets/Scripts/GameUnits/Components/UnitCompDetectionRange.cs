﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompDetectionRange : AbstractComponentJH3 {

    [SerializeField]
    private float _detectionRadius = 2.0f;

    public float detectionRadius
    {
        get { return _detectionRadius; }
        set { _detectionRadius = Mathf.Abs(value); }
    }

    private void OnValidate()
    {
        detectionRadius = detectionRadius;
    }
}

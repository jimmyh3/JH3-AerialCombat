﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[System.Serializable]
public class UnitCompRotateSpeed : AbstractComponentJH3
{
    // Variables
    [SerializeField]
    [Tooltip("Speed of the rotation in degrees. \n"
                + "Note that radians input and degrees input are linked.")]
    private float _degrees = 120f;

    [SerializeField]
    [Tooltip("Speed of the rotation in radians. \n"
                + "Note that radians input and degrees input are linked.")]
    private float _radians = 2.094396f;
    
    private float validDeg;     // Used for OnValidate() only.
    private float validRad;     // Used for OnValidate() only.


    // Properties

    /// <summary>  
    /// Rotation speed in degrees.
    /// Note that the degrees are modulus of 360.
    /// This is equal to the variable <see cref="radians"/>.
    /// </summary>
    public float degrees
    {
        get { return _degrees; }
        set
        {
            validDeg = _degrees;
            _degrees = value % 360f;
            _radians = (_degrees * Mathf.Deg2Rad) % (2 * Mathf.PI);
        }
    }

    /// <summary>
    /// Rotation speed in radians.
    /// Note that the radians are modulus of (2 * pi).
    /// This is equal to the variable <see cref="degrees"/>.
    /// </summary>
    public float radians
    {
        get { return _radians; }
        set
        {
            validRad = _radians;
            _radians = value % (2 * Mathf.PI);
            _degrees = (_radians * Mathf.Rad2Deg) % (360f);
        }
    }

    
    // Unity Methods
    public void OnValidate()
    {
        if (!Mathf.Approximately(validDeg, degrees)) degrees = degrees;
        if (!Mathf.Approximately(validRad, radians)) radians = radians;
    }

}

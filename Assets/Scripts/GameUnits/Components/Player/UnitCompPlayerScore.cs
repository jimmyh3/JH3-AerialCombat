﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class UnitCompPlayerScore : AbstractComponentJH3 {
    
    public int scorePointValue;
    
}

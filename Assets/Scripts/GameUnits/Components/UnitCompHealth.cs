﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class UnitCompHealth : AbstractComponentJH3 {

    ///// <summary><see cref="ObservableJH3"/> for <see cref="ObserverJH3"/>s to watch <see cref="currentHealth"/>.</summary>
    //public ObservableJH3 currentHealthObservable = new ObservableJH3();

    public delegate void CurrentHealthCallBack(float value);
    public event CurrentHealthCallBack OnCurrentHealthModified;

    // **** Public/Inspector Fields ****
    [Tooltip("Setting invincibility to true will negate any further health lost.")]
    public bool isInvincibile = false;
    
    [Tooltip("Current health of this unit.")]
    [SerializeField]
    private float _currentHealth = 100.0f;

    [Tooltip("Maximum health of this unit.")]
    [SerializeField]
    private float _maximumHealth = 100.0f;

    [Tooltip("Color to flash upon taking damage.")]
    public Color onHitColor = Color.red;
    
    // **** Private Fields ****
    private List<Renderer> renderers;           // Caching this GameObject Renderers
    private List<Color> originalColors;         // Saving original colors belonging to the materials of the renderer.


    // **********************
    //
    //      Properties
    //
    // **********************

    /// <summary>
    /// Current health of this unit. Normally this value is clamped between 
    /// ZERO and <see cref="maximumHealth"/>, unless <see cref="isInvincibile"/> is true,
    /// in which case the current health is non-decreasing.
    /// </summary>
    public float currentHealth
    {
        get { return _currentHealth; }
        private set { _currentHealth = Mathf.Clamp(value, 0.0f, maximumHealth); }
    }
    
    /// <summary>
    /// Maximum allowed health of this unit.
    /// The value clamped between ZERO and <see cref="float.MaxValue"/>.
    /// </summary>
    public float maximumHealth
    {
        get { return _maximumHealth; }
        set { _maximumHealth = Mathf.Clamp(value, 0.0f, float.MaxValue); }
    }

    /// <summary>
    /// Check marker to determine if this object has reached zero health before.
    /// </summary>
    public bool isDead { get; set; }

    // **********************
    //
    //      Unity Methods
    //
    // **********************

    /// <summary>
    /// Ensures <see cref="currentHealth"/> and <see cref="maximumHealth"/> is within
    /// allowed values.
    /// </summary>
    protected override void OnValidate()
    {
        base.OnValidate();

        currentHealth = currentHealth;
        maximumHealth = maximumHealth;
    }

    private void Start()
    {
        isDead = false;
        renderers = new List<Renderer>(GetComponentsInChildren<Renderer>());
        originalColors = new List<Color>();

        for (int i = 0; i < renderers.Count; i++)
        { 
            if (renderers[i].material.HasProperty("_Color"))
            {
                originalColors.Add(renderers[i].material.color);
            }
            else
            {
                renderers.RemoveAt(i);
                i = i - 1;
            }
        }
    }

    // **********************
    //
    //      Public Methods
    //
    // **********************

    public void ReceiveHealth(float value)
    {
        currentHealth += value;

        if (OnCurrentHealthModified != null)
            OnCurrentHealthModified(currentHealth);
    }

    public void ReceiveDamage(float damage)
    {
        if (isInvincibile) return;

        currentHealth -= damage;

        StartCoroutine(OnHitColorPlay(0.3f));

        if (OnCurrentHealthModified != null)
            OnCurrentHealthModified(currentHealth);
    }

    // **********************
    //
    //      Private Methods
    //
    // **********************

    /// <summary>
    /// Flashes the <see cref="Material.color"/> to the given color <see cref="onHitColor"/>.
    /// </summary>
    /// <param name="colorDuration">Wait time before changing the color back to its default.</param>
    /// <returns></returns>
    private IEnumerator OnHitColorPlay(float colorDuration)
    {
        foreach (Renderer renderer in renderers)
            renderer.material.color = onHitColor;

        yield return new WaitForSeconds(colorDuration);

        for (int i = 0; i < renderers.Count; i++)
            renderers[i].material.color = originalColors[i];
    }
    
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Jimmy He
 * JH3 - Aerial Combat
 */
 [DisallowMultipleComponent]
[RequireComponent(typeof(Aircraft))]
public class UnitCtrlPlayerAircraft : MonoBehaviour, IPlayerController
{

    // ********* Variables **************
    private Aircraft _aircraft;

    // ********* Properties *************
    private Aircraft aircraft
    {
        get
        {
            if (_aircraft == null)
                _aircraft = GetComponent<Aircraft>();
            return _aircraft;
        }
    }


    // **********************************
    // 
    //          Unity Methods
    //
    // **********************************

    private void OnValidate()
    {
        if (aircraft == null)
        {
            _aircraft = gameObject.AddComponent<Aircraft>();
            Debug.LogWarning("PlayerAircraftController: Aircraft script was missing but has been added!", this);
        }
    }

    private void Start()
    {
        OnValidate();
    }

    // **********************************
    // 
    //      Public/Protected Methods
    //
    // **********************************

    public void HandleMouseClick(Vector3 mousePosition)
    {
        MoveToward(mousePosition);
        FireWeapons();
    }

    /// <summary>
    /// Constantly fire weapons in <see cref="weaponList"/> when able.
    /// In the case of the Player aircraft the weapons are always firing when able.
    /// </summary>
    public void FireWeapons()
    {
        foreach (Weapon x in aircraft.weaponList)
            x.Fire();
    }

    /// <summary>
    /// Move this object closer toward the given target position.
    /// This uses the <see cref="UnitCompMoveSpeed"/> values to limit velocity and accelerate.
    /// </summary>
    /// <param name="newTargetPosition">Vector3 position of where to move this object to.</param>
    public void MoveToward(Vector3 newTargetPosition)
    {
        MoveToward_UpdateVelocityDirection(newTargetPosition);
        MoveToward_UpdateAccelerationState(newTargetPosition);
        MoveToward_UpdateVelocityAcceleration(newTargetPosition);
        MoveToward_UpdateMoveTilt();
    }

    private void MoveToward_UpdateVelocityDirection(Vector3 newTargetPosition)
    {
        if (aircraft.rigidbody == null) return;
        if (aircraft.rigidbody.position == newTargetPosition) return;


        Vector3 newVelocityDirection;

        newVelocityDirection = (newTargetPosition - transform.position).normalized;

        // Set the velocity magnitude to new direction if velocity is greater than zero.
        if (!MathJH3.FloatApproximately(aircraft.rigidbody.velocity.magnitude, 0.0f, 0.05f))
            newVelocityDirection *= aircraft.rigidbody.velocity.magnitude;

        aircraft.rigidbody.velocity = newVelocityDirection;
    }

    private void MoveToward_UpdateAccelerationState(Vector3 newTargetPosition)
    {
        if (aircraft.rigidbody == null || aircraft.moveSpeedComponent == null) return;

        float newTargetDistance;
        float curDecelerationDistance;      // distance required to get to zero velocity 
        float maxDecelerationDistance;

        newTargetDistance = Vector3.Distance(transform.position, newTargetPosition);

        // v_f^2 = v_i^2 + 2*a*d;
        // v_f^2 == 0 for this equation; -2 == 2 as maxDeceleration is implied negative.
        curDecelerationDistance = Mathf.Pow(aircraft.rigidbody.velocity.magnitude, 2.0f)
                                         / (2 * aircraft.moveSpeedComponent.maxDeceleration);
        maxDecelerationDistance = Mathf.Pow(aircraft.moveSpeedComponent.maxSpeed, 2.0f)       
                                         / (2 * aircraft.moveSpeedComponent.maxDeceleration);

        // if not accelerating in any way && if not top velocity && if target not zero close, then accelerate to target.
        if ((aircraft.moveSpeedComponent.isAccelerating == 0.0f)
                && (!MathJH3.FloatApproximately(newTargetDistance, 0.0f, 0.2f))
                    && (aircraft.rigidbody.velocity.magnitude < aircraft.moveSpeedComponent.maxSpeed))
        {
            aircraft.moveSpeedComponent.isAccelerating = 1.0f;
        }
        else
        // if not decelerating && curVelocity is about max velocity or greater && if target distance is less than or equal to max decel distance, then decelerate.
        if ((aircraft.moveSpeedComponent.isAccelerating >= 0.0f)
                && (newTargetDistance <= maxDecelerationDistance)
                    && ( (aircraft.rigidbody.velocity.magnitude >= aircraft.moveSpeedComponent.maxSpeed)
                    ||   (MathJH3.FloatApproximately(aircraft.rigidbody.velocity.magnitude, aircraft.moveSpeedComponent.maxSpeed, 1.0f))) )
        {
            aircraft.moveSpeedComponent.isAccelerating = -1.0f;
        }
        else
        // If is decelerating && target position has moved greater than calculated current deceleration distance...
        if ((aircraft.moveSpeedComponent.isAccelerating < 0.0f) && (newTargetDistance > curDecelerationDistance))
        {
            aircraft.moveSpeedComponent.isAccelerating = 1.0f;
        }

        if ((aircraft.moveSpeedComponent.isAccelerating != 0.0f) && (MathJH3.FloatApproximately(newTargetDistance, 0.0f, 0.2f)))
        {
            aircraft.moveSpeedComponent.isAccelerating = 0.0f;
        }
    }

    private void MoveToward_UpdateVelocityAcceleration(Vector3 newTargetPosition)
    {
        if (aircraft.rigidbody == null || aircraft.moveSpeedComponent == null) return;

        // Return if current position is approximately close to target destination.
        if (MathJH3.FloatApproximately(Vector3.Distance(newTargetPosition, transform.position),
                                0.0f,
                                aircraft.rigidbody.velocity.magnitude * Time.fixedDeltaTime))
        {
            aircraft.rigidbody.velocity = Vector3.zero;
            aircraft.rigidbody.position = newTargetPosition;

            aircraft.moveSpeedComponent.isAccelerating = 0.0f;

            return;
        }


        // If distance to target is greater than zero and the state is accelerating, then add acceleration force.
        if ((Vector3.Distance(transform.position, newTargetPosition) > 0.0f) && (aircraft.moveSpeedComponent.isAccelerating > 0.0f))
        {
            aircraft.rigidbody.AddForce(aircraft.rigidbody.velocity.normalized * aircraft.moveSpeedComponent.maxAcceleration, ForceMode.Acceleration);
        }
        // If distance to target is greater than zero and the state is deceleration, then add deceleration force.
        else if ((Vector3.Distance(transform.position, newTargetPosition) > 0.0f) && (aircraft.moveSpeedComponent.isAccelerating < 0.0f))
        {
            aircraft.rigidbody.AddForce(-aircraft.rigidbody.velocity.normalized * aircraft.moveSpeedComponent.maxAcceleration, ForceMode.Acceleration);
        }

        // If current velocity is close to max velocity by given margin then set it to max velocity.
        if ((aircraft.rigidbody.velocity.magnitude >= aircraft.moveSpeedComponent.maxSpeed)
                || (MathJH3.FloatApproximately(aircraft.rigidbody.velocity.magnitude, aircraft.moveSpeedComponent.maxSpeed, aircraft.moveSpeedComponent.maxAcceleration * Time.fixedDeltaTime)))
        {
            aircraft.rigidbody.velocity = aircraft.rigidbody.velocity.normalized * aircraft.moveSpeedComponent.maxSpeed;
        }

        // If current velocity is close to zero then set it to zero.
        if (MathJH3.FloatApproximately(aircraft.rigidbody.velocity.magnitude, 0.0f, aircraft.moveSpeedComponent.maxAcceleration * Time.fixedDeltaTime))
        {
            aircraft.rigidbody.velocity = Vector3.zero;
        }
    }

    /// <summary>
    /// Tilts the plane according to the direction of velocity vector.
    /// </summary>
    /// <param name="velocityVector"></param>
    private void MoveToward_UpdateMoveTilt()
    {
        if (aircraft.rigidbody == null || aircraft.moveSpeedComponent == null || aircraft.moveTiltComponent == null) return;

        /*
         * Approach:
         * Interpolate current rotation toward new determined rotation.
         *      The new determined rotation is calculated by current X-axis velocity over maximum velocity
         *      multiplied by maximum allowed tilt in the Z-axis to get how far the object should be tilted.
         *      The object is then tilted towards the new rotation at a max rate of tilt for the Z angle.
         */

        float floatApproxMargin;
        Quaternion newRotationQuaternion;
        Vector3 newRotationEuler;

        newRotationEuler = aircraft.rigidbody.rotation.eulerAngles;
        newRotationEuler.z = (aircraft.rigidbody.velocity.x / aircraft.moveSpeedComponent.maxSpeed)
                                                    * aircraft.moveTiltComponent.maxTiltAngleZ
                                                    * -1;
        floatApproxMargin = (aircraft.rigidbody.velocity.x / aircraft.moveSpeedComponent.maxSpeed)
                                                    * aircraft.moveTiltComponent.maxRateOfTiltZ
                                                    * Time.fixedDeltaTime;


        // Set new rotation to aircraft.rigidbody.rotation
        if (Quaternion.Angle(aircraft.rigidbody.rotation, Quaternion.Euler(newRotationEuler)) < floatApproxMargin)
        {
            newRotationQuaternion = aircraft.rigidbody.rotation;
        }
        else
        {
            newRotationQuaternion = Quaternion.RotateTowards(aircraft.rigidbody.rotation,
                                                             Quaternion.Euler(newRotationEuler),
                                                             aircraft.moveTiltComponent.maxRateOfTiltZ * Time.fixedDeltaTime);
        }

        aircraft.rigidbody.rotation = newRotationQuaternion;
        
    }
    
}

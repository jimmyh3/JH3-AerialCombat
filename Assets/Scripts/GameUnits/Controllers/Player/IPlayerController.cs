﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerController {
    
    void HandleMouseClick(Vector3 mousePosition);

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCtrlPlayerScoreAdder : MonoBehaviour {

    // ************************
    //
    //      Unity Methods
    //
    // ************************

    private void OnTriggerEnter(Collider other)
    {
        AddScoreProcess(other.GetComponentInParent<GameUnit>());
    }

    private void OnCollisionEnter(Collision collision)
    {
        AddScoreProcess(collision.gameObject.GetComponentInParent<GameUnit>());
    }

    // ************************
    //
    //      Private Methods
    //
    // ************************

    private void AddScoreProcess(GameUnit other)
    {
        if (other == null) return;

        if (HasZeroHealthRemaining(other))
            AddScore(other);
    }

    private bool HasZeroHealthRemaining(GameUnit other)
    {
        if (other == null) return false;

        bool hasZeroHealth = false;
        UnitCompHealth otherHealthComponent;

        otherHealthComponent = other.GetComponent<UnitCompHealth>();

        if (otherHealthComponent != null)
            hasZeroHealth = (otherHealthComponent.currentHealth <= 0.0f);

        return hasZeroHealth;
    }

    private void AddScore(GameUnit other)
    {
        AddScore(other.GetComponent<UnitCompPlayerScore>());
    }

    private void AddScore(UnitCompPlayerScore _playerScoreComponent)
    {
        if (_playerScoreComponent != null)
            AddScore(_playerScoreComponent.scorePointValue);
    }

    private void AddScore(int value)
    {
        PlayerManager.Instance.playerCurrentScore += value;
    }

}

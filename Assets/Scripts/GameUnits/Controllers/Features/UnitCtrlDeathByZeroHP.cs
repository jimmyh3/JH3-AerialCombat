﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class UnitCtrlDeathByZeroHP : MonoBehaviour {
    
    // ***** Private Fields *****
    private UnitCompHealth _healthComponent;
    private UnitCompDeathZeroHP _deathZeroHPComponent;

    // ***** Properties *****
    public UnitCompHealth healthComponent
    {
        get
        {
            if (_healthComponent == null)
                _healthComponent = GetComponent<UnitCompHealth>();
            
            return _healthComponent;
        }
    }

    public UnitCompDeathZeroHP deathZeroHPComponent
    {
        get
        {
            if (_deathZeroHPComponent == null)
                _deathZeroHPComponent = GetComponent<UnitCompDeathZeroHP>();

            return _deathZeroHPComponent;
        }
    }

    // ************************
    //
    //      Unity Methods
    //
    // ************************

    private void OnValidate()
    {
        // Warn if there is more than one instance on this GameObject
        if (GetComponents(this.GetType()).Length > 1)
            Debug.LogError(this.GetType().Name + ": requires only one instance on this GameObject!", this);
    }

    private void Start()
    {
        if (_healthComponent == null)
            _healthComponent = GetComponent<UnitCompHealth>();
        
        IsDeadProcess(_healthComponent.currentHealth);
    }
    

    private void OnEnable()
    {
        if (healthComponent != null)
            healthComponent.OnCurrentHealthModified += IsDeadProcess;
    }

    private void OnDisable()
    {
        if (healthComponent != null)
            healthComponent.OnCurrentHealthModified -= IsDeadProcess;
    }
    

    // ************************
    //
    //      Public Methods
    //
    // ************************
    
    /// <summary>
    /// If the given health is less than or equal to zero, then the object of which
    /// the <see cref="UnitCompHealth"/> is attached to is effectively dead. Properties of
    /// the <see cref="UnitCompDeathZeroHP"/> are then used to determine how the death is played out.
    /// </summary>
    /// <param name="currentHealth">The value check to determine if living or dead.</param>
    private void IsDeadProcess(float currentHealth)
    {
        if (currentHealth <= 0.0f && deathZeroHPComponent != null)
        {
            if (deathZeroHPComponent.instantiateOnDeath != null)
                Instantiate(deathZeroHPComponent.instantiateOnDeath, this.transform.position, this.transform.rotation);

            if (deathZeroHPComponent.isDestroyable)
                Destroy(deathZeroHPComponent.gameObject);
        }
    }


}

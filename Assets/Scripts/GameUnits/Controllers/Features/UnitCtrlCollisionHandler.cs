﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <see cref="UnitCtrlCollisionHandler"/> handles basic collisions between
/// two <see cref="GameUnit"/>s. It utilizes the <see cref="GameUnit"/> of which
/// this resides on to find its <see cref="UnitCompDamageCollision"/> and apply its
/// <see cref="UnitCompDamageCollision.damage"/> towards the opposing 
/// <see cref="GameUnit"/>'s <see cref="UnitCompHealth.currentHealth"/>.
/// </summary>
[DisallowMultipleComponent]
public class UnitCtrlCollisionHandler : MonoBehaviour {

    // ***** Private Fields *****
    private GameUnit _selfGameUnit;
    private UnitCompDamageCollision _damageCollisionComponent;

    // ***** Properties *****
    public GameUnit selfGameUnit
    {
        get
        {
            if (_selfGameUnit == null)
                _selfGameUnit = GetComponent<GameUnit>();

            return _selfGameUnit;
        }
    }

    public UnitCompDamageCollision damageCollisionComponent
    {
        get
        {
            if (_damageCollisionComponent == null)
                _damageCollisionComponent = GetComponent<UnitCompDamageCollision>();

            return _damageCollisionComponent;
        }
    }

    // **********************
    //
    //      Unity Methods
    //
    // **********************

    protected virtual void OnValidate()
    {
        if (damageCollisionComponent == null)
            Debug.LogWarning(this.name + ": DamageCollisionComponent is missing!", this);

        // Warn if there is more than one instance on this GameObject
        if (GetComponents(this.GetType()).Length > 1)
            Debug.LogError(this.GetType().Name + ": requires only one instance on this GameObject!", this);

    }

    protected virtual void Awake()
    {
        _damageCollisionComponent = GetComponent<UnitCompDamageCollision>();
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        UnitCollisionProcess(other.GetComponentInParent<GameUnit>());
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        UnitCollisionProcess(collision.gameObject.GetComponentInParent<GameUnit>());
    }

    // **************************
    //
    //      Protected Methods
    //
    // **************************

    protected virtual void UnitCollisionProcess(GameUnit otherGameUnit)
    {
        if (otherGameUnit == null) return;

        ProcessHealthDamages(otherGameUnit);
    }

    protected virtual void ProcessHealthDamages(GameUnit otherGameUnit)
    {
        if (otherGameUnit == null) return;
        
        if (GameUnitFriendFoeManager.Instance.IsEnemy(selfGameUnit, otherGameUnit))
        {
            UnitCompHealth otherHealthComp = otherGameUnit.GetComponent<UnitCompHealth>();

            if (otherHealthComp != null && damageCollisionComponent != null)
                otherHealthComp.ReceiveDamage(damageCollisionComponent.damage);
        }
    }

}

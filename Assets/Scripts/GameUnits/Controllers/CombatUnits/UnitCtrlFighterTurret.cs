﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Turret))]
public class UnitCtrlFighterTurret : MonoBehaviour {

    // ********* Variables **************
    private Turret _turret;

    // ********* Properties **************
    public Turret turret
    {
        get
        {
            if (_turret == null)
                _turret = GetComponent<Turret>();

            if (_turret == null)
                _turret = gameObject.AddComponent<Turret>();

            return _turret;
        }
    }
    

    // **********************************
    // 
    //          Unity Methods
    //
    // **********************************

    private void OnValidate()
    {
        if (turret == null)
        {
            _turret = gameObject.AddComponent<Turret>();
            Debug.LogWarning(this.GetType().Name + ": Turret script was missing but has been added!", this);
        }

        // Warn if there is more than one instance on this GameObject
        if (GetComponents(this.GetType()).Length > 1)
            Debug.LogError(this.GetType().Name + ": requires only one instance on this GameObject!", this);

    }

    // **********************************
    // 
    //          Public Methods
    //
    // **********************************

    /// <summary>
    /// Determine if the given<see cref= "GameUnit" /> target is in sight range.
    /// The range is determined by the<see cref="Aircraft.sightRangeComponent"/> values.
    /// </summary>
    /// <param name = "target" > The < see cref= "GameUnit" /> to check if still within range.</param>
    /// <returns><see cref = "bool" /></ returns >
    public bool IsTargetInSightRange(GameUnit target)
    {
        if (turret.detectionRangeComponent == null || target == null) return false;
        
        float sightRange;
        float distanceToTarget;
        Vector3 directionToTarget;

        sightRange = turret.detectionRangeComponent.detectionRadius;
        distanceToTarget = Vector3.Distance(gameObject.transform.position, target.transform.position);
        directionToTarget = (target.transform.position - gameObject.transform.position).normalized;
        
        // Exit 1: true if exact position of target is less distance away than sight range.
        if (distanceToTarget < sightRange)
            return true;
        
        // Exit 2: true if one of target's body colliders is in sight range.
        RaycastHit[] raycastHits;
        raycastHits = Physics.RaycastAll(gameObject.transform.position, directionToTarget, sightRange);

        foreach (RaycastHit raycastHit in raycastHits)
            foreach (Collider bodyCollider in target.bodyColliders)
                if (bodyCollider == raycastHit.collider)
                    return true;
        
        // Exit 3: false by default.
        return false;
    }

    /// <summary>
    /// Determine if the given <see cref="GameUnit"/> is directly ahead of this caller along its forward Z-axis.
    /// </summary>
    /// <param name="target"><see cref="GameUnit"/> to determine the distance away.</param>
    /// <returns><see cref="float"/>. Positive value = distance away. Negative = target is not ahead at all.</returns>
    public float IsTargetDirectlyAhead(GameUnit target)
    {
        if (target == null) return -1.0f;

        float distanceAway;         // How far away the target is if it is in range.
        RaycastHit[] raycastHits;   // Used to check if target GameUnit is within range ahead.
        
        distanceAway = -1.0f;
        raycastHits = Physics.RaycastAll(turret.turretFace.transform.position, 
                                            turret.turretFace.transform.forward, 
                                                turret.detectionRangeComponent.detectionRadius);
        
        // Check if any of target's colliders is in range, if so, get its distance.
        foreach (RaycastHit raycastHit in raycastHits)
        {
            GameUnit maybeTarget = raycastHit.collider.GetComponentInParent<GameUnit>();
            
            if (maybeTarget == target)
            {
                distanceAway = Vector3.Distance(transform.position, raycastHit.point);
                break;
            }
        }

        return distanceAway;
    }

    /// <summary>
    /// Get the closest enemy <see cref="GameUnit"/> in range of the turret head's direction. The range is determined by <see cref="UnitCompDetectionRange"/>
    /// while the enemy or friend identification is done through <see cref="UnitCompFriendFoe"/> and <see cref="GameUnitFriendFoeManager.IsEnemy(GameUnit, GameUnit)"/>.
    /// </summary>
    /// <returns>The closest enemy<see cref="GameUnit"/>.</returns>
    public GameUnit GetTargetInSightRange()
    {
        Collider[] targetsInRange;      // For Physics.OverlapSphere()
        GameUnit currentTarget = null;  // Returns this item.
        
        targetsInRange = Physics.OverlapSphere(transform.position, turret.detectionRangeComponent.detectionRadius);
        
        /*
         * Get the closest unit based on what's in range and closest to the turret head's forward direction.
         */
        foreach (Collider targetCollider in targetsInRange)
        {
            GameUnit possibleEnemy = targetCollider.GetComponentInParent<GameUnit>();
            bool isTargetEnemy = GameUnitFriendFoeManager.Instance.IsEnemy(turret, possibleEnemy);

            if (!isTargetEnemy)
                continue;

            if (currentTarget == null)
                currentTarget = possibleEnemy;

            // For cases of compound colliders from one GameUnit
            if (currentTarget == possibleEnemy)
                continue;
            
            float angleToCurTarget = Vector3.Angle(currentTarget.transform.position - transform.position, turret.turretHead.transform.forward);
            float angleToNextTarget = Vector3.Angle(possibleEnemy.transform.position - transform.position, turret.turretHead.transform.forward);

            if (angleToNextTarget < angleToCurTarget) currentTarget = possibleEnemy;
        }

        return currentTarget;
    }
    
    /// <summary>
    /// Fire weapons in <see cref="Turret.weaponList"/>. Note that whether or not the
    /// <see cref="Weapon"/> can fire is dependent on its <see cref="Weapon.rateOfFire"/>.
    /// </summary>
    public void FireWeapons()
    {
        foreach (Weapon x in turret.weaponList)
        {
            x.Fire();
        }
    }

    /// <summary>Rotate toward the turret at <see cref="UnitCompRotateSpeed.degrees"/>
    /// toward the given <see cref="GameUnit"/> target. This controls both the turret stem
    /// and turret barrel as well.</summary>
    /// <param name="position">Target position to rotate toward to.</param>
    public void RotateToward(GameUnit target)
    {
        if (turret.rotateSpeedComponent == null || target == null) return;

        RotateToward_TurretHead(target);
        RotateToward_TurretFace(target);
    }

    /// <summary>
    /// Helper method of <see cref="RotateToward(GameUnit)"/> which rotates the 
    /// <see cref="Turret.turretHead"/> to face toward the given targeted <see cref="GameUnit"/>.
    /// </summary>
    /// <param name="target">The target to rotate the turret stem toward.</param>
    private void RotateToward_TurretHead(GameUnit target)
    {
        if (turret.turretHead == null || turret.turretHead.activeSelf == false || turret.rotateSpeedComponent == null || target == null) return;

        Plane planeLocalYAxis;          // Plane representing the turret stem's local Y-axis (aka horizontal rotation).
        float planeToTargetDist;        // Distance from a perpendicular point on the plane to target's position.
        Vector3 planePerpendicularPt;   // Perpendicular point on the plane to target's position.
        Quaternion newRotation;         // The rotation to be made.

        planeLocalYAxis = new Plane(turret.turretHead.transform.up, turret.turretHead.transform.position);
        planeToTargetDist = planeLocalYAxis.GetDistanceToPoint(target.transform.position);
        planePerpendicularPt = target.transform.position + (-turret.turretHead.transform.up * planeToTargetDist);
        
        newRotation = Quaternion.RotateTowards( turret.turretHead.transform.rotation,
                                                Quaternion.LookRotation(planePerpendicularPt - turret.turretHead.transform.position),
                                                turret.rotateSpeedComponent.degrees * Time.deltaTime);

        float x = turret.turretHead.transform.localEulerAngles.x;
        float z = turret.turretHead.transform.localEulerAngles.z;

        turret.turretHead.transform.rotation = newRotation;
        turret.turretHead.transform.localEulerAngles = new Vector3(x, turret.turretHead.transform.localEulerAngles.y, z);
    }

    /// <summary>
    /// Helper method of <see cref="RotateToward(GameUnit)"/> which aims the <see cref="Turret"/>
    /// barrel up and down toward the given targeted <see cref="GameUnit"/>.
    /// </summary>
    /// <param name="target">The target to rotate the barrel up/down toward.</param>
    private void RotateToward_TurretFace(GameUnit target)
    {
        if (turret.turretHead == null || turret.turretHead.activeSelf == false || turret.turretFace == null
                                ||  turret.turretFace.activeSelf == false || turret.rotateSpeedComponent == null 
                                                            || target == null || !target.gameObject.activeSelf) return;

        float angleOfTarget;            // Angle from turret stem to target.
        float angleOfBarrel;            // Angle from turret stem to turret barrel.
        float angleAdjustForBarrel;     // AngleOfTarget - AngleOfBarrel.
        float faceRotateAmount;         // Is given rotate speed times deltaTime unless angleAdjustForBarrel is smaller.

        
        // Idea: Use the turret's up direction to determine the vertical rotation difference from barrel to target.
        angleOfTarget = Vector3.Angle(turret.turretHead.transform.up, target.transform.position - turret.turretHead.transform.position);
        angleOfBarrel = Vector3.Angle(turret.turretHead.transform.up, turret.turretFace.transform.forward);
        angleAdjustForBarrel = angleOfTarget - angleOfBarrel;


        if (Mathf.Abs(angleAdjustForBarrel) < (turret.rotateSpeedComponent.degrees * Time.deltaTime))
            faceRotateAmount = angleAdjustForBarrel;
        else
            faceRotateAmount = (turret.rotateSpeedComponent.degrees * Time.deltaTime) * Mathf.Sign(angleAdjustForBarrel);

        float x = turret.turretFace.transform.localEulerAngles.x;
        float y = turret.turretFace.transform.localEulerAngles.y;
        float z = turret.turretFace.transform.localEulerAngles.z;
        
        turret.turretFace.transform.localEulerAngles = new Vector3(x + faceRotateAmount, y, z);
        
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Aircraft))]
public class UnitCtrlFighterAircraft : MonoBehaviour
{

    // ********* Variables **************
    private Aircraft _aircraft;

    // ********* Properties **************
    public Aircraft aircraft
    {
        get
        {
            if (_aircraft == null)
                _aircraft = GetComponent<Aircraft>();

            if (_aircraft == null)
                _aircraft = gameObject.AddComponent<Aircraft>();

            return _aircraft;
        }
    }


    // **********************************
    // 
    //          Unity Methods
    //
    // **********************************
    
    private void OnValidate()
    {
        // Warn if there is more than one instance on this GameObject
        if (GetComponents(this.GetType()).Length > 1)
            Debug.LogError(this.GetType().Name + ": requires only one instance on this GameObject!", this);

        if (aircraft == null)
        {
            _aircraft = gameObject.AddComponent<Aircraft>();
            Debug.LogWarning("FighterAircraftController: Aircraft script was missing but has been added!", this);
        }
    }

    // **********************************
    // 
    //          Public Methods
    //
    // **********************************

    /// <summary>
    /// Determine if the given <see cref="GameUnit"/> target is in range directly ahead.
    /// The range is determined by the <see cref="Aircraft.sightRangeComponent"/> values.
    /// </summary>
    /// <param name="target">The <see cref="GameUnit"/> to check if still within range.</param>
    /// <returns><see cref="bool"/></returns>
    public bool IsTargetInSightRange(GameUnit target)
    {
        if (aircraft.sightRangeComponent == null || target == null) return false;

        Bounds sightBounds = new Bounds();
        
        sightBounds.center = transform.position + (transform.forward * (aircraft.sightRangeComponent.maxDistance / 2.0f));
        sightBounds.extents = new Vector3( aircraft.sightRangeComponent.halfWidthX,
                                                aircraft.sightRangeComponent.halfHeightY,
                                                aircraft.sightRangeComponent.maxDistance / 2.0f );

        foreach (Collider collider in target.bodyColliders)
            if (sightBounds.Intersects(collider.bounds))
                return true;
        
        return false;
    }
    
    /// <summary>
    /// Determine if the given <see cref="GameUnit"/> is directly ahead of this caller along its forward Z-axis.
    /// </summary>
    /// <param name="target"><see cref="GameUnit"/> to determine the distance away.</param>
    /// <returns><see cref="float"/>. Positive value = distance away. Negative = target is not ahead at all.</returns>
    public float IsTargetDirectlyAhead(GameUnit target)
    {
        if (target == null) return -1.0f;

        float distanceAway = -1.0f;
        Ray forwardRay = new Ray(transform.position, transform.forward);
        
        foreach (Collider collider in target.bodyColliders)
            if (collider.bounds.IntersectRay(forwardRay, out distanceAway))
                return distanceAway;

        return distanceAway;
    }

    /// <summary>
    /// Get the closest enemy <see cref="GameUnit"/> in range. The range is determined by <see cref="UnitCompSightRange"/>
    /// while the enemy or friend identification is done through <see cref="UnitCompFriendFoe"/> and <see cref="GameUnitFriendFoeManager.IsEnemy(GameUnit, GameUnit)"/>.
    /// </summary>
    /// <returns>The closest enemy<see cref="GameUnit"/>.</returns>
    public GameUnit GetTargetInSightRange()
    {
        GameUnit firstTargetInRange = null;

        if (!(aircraft.sightRangeComponent == null || aircraft.idFriendFoeComponent == null))
        {
            float maxDistance;
            Vector3 halfExtents;
            RaycastHit[] raycastHits;

            maxDistance = aircraft.sightRangeComponent.maxDistance;
            halfExtents = new Vector3(aircraft.sightRangeComponent.halfWidthX, aircraft.sightRangeComponent.halfHeightY, 0.0f);
            raycastHits = Physics.BoxCastAll(transform.position, halfExtents, transform.forward, transform.rotation, maxDistance);

            foreach (RaycastHit raycastHit in raycastHits)
            {
                GameUnit gameUnit = raycastHit.collider.GetComponentInParent<GameUnit>();

                if ((gameUnit != null) && (GameUnitFriendFoeManager.Instance.IsEnemy(aircraft, gameUnit)))
                {
                    firstTargetInRange = gameUnit;
                    break;
                }
            }
        }

        return firstTargetInRange;
    }

    /// <summary>
    /// Get a list of enemy <see cref="GameUnit"/> in range. The range is determined by <see cref="UnitCompSightRange"/>
    /// and the enemy or friend identification is done through <see cref="UnitCompFriendFoe"/> and <see cref="GameUnitFriendFoeManager.IsEnemy(GameUnit, GameUnit)"/>.
    /// </summary>
    /// <returns>The closest enemy<see cref="GameUnit"/>.</returns>
    public List<GameUnit> GetTargetsInSightRange()
    {
        Dictionary<int, GameUnit> targetsInRangeDict = new Dictionary<int, GameUnit>();
         
        if (!(aircraft.sightRangeComponent == null || aircraft.idFriendFoeComponent == null))
        {
            float maxDistance;
            Vector3 halfExtents;
            RaycastHit[] raycastHits;

            maxDistance = aircraft.sightRangeComponent.maxDistance;
            halfExtents = new Vector3(aircraft.sightRangeComponent.halfWidthX, aircraft.sightRangeComponent.halfHeightY, 0.0f);
            raycastHits = Physics.BoxCastAll(transform.position, halfExtents, transform.forward, transform.rotation, maxDistance);

            foreach (RaycastHit raycastHit in raycastHits)
            {
                GameUnit gameUnit = raycastHit.collider.GetComponentInParent<GameUnit>();
                
                if ((gameUnit != null)
                        && (GameUnitFriendFoeManager.Instance.IsEnemy(aircraft, gameUnit))
                        && (!targetsInRangeDict.ContainsKey(gameUnit.GetInstanceID())) )
                {
                    targetsInRangeDict[gameUnit.GetInstanceID()] = gameUnit;
                }
            }
        }
        
        return targetsInRangeDict.Values.ToList<GameUnit>();
    }

    /// <summary>
    /// Fire weapons in <see cref="Aircraft.weaponList"/>. Note that whether or not the
    /// <see cref="Weapon"/> can fire is dependent on its <see cref="Weapon.rateOfFire"/>.
    /// </summary>
    public void FireWeapons()
    {
        foreach (Weapon x in aircraft.weaponList)
            x.Fire();
    }

    /// <summary>Moves the unit forward based on its local Z-axis direction.</summary>
    public void MoveForward()
    {
        if (aircraft.moveSpeedComponent == null) return;

        transform.position += transform.forward * aircraft.moveSpeedComponent.currentSpeed * Time.deltaTime;
    }

    /// <summary>
    /// Moves toward the given <see cref="GameUnit"/>. This utilizes the <see cref="Aircraft"/>,
    /// <see cref="UnitCompMoveSpeed"/> and <see cref="UnitCompRotateSpeed"/>
    /// </summary>
    /// <param name="position">Target <see cref="GameUnit"/> to move to.</param>
    public void MoveToward(GameUnit target)
    {
        MoveToward(target.transform.position);
    }

    /// <summary>
    /// Moves toward the given position. This utilizes the <see cref="Aircraft"/>,
    /// <see cref="UnitCompMoveSpeed"/> and <see cref="UnitCompRotateSpeed"/>
    /// </summary>
    /// <param name="position">Target position to move to.</param>
    public void MoveToward(Vector3 position)
    {
        MoveToward_AdjustSpeed(position);
        MoveToward_Forward(position);
        MoveToward_Rotate(position);
        MoveToward_Tilt(position);
    }

    /// <summary>
    /// Determine if increasing or decreasing speed is necessary for this object to intercept the
    /// given position. This uses the <see cref="Aircraft"/> <see cref="UnitCompRotateSpeed"/> and
    /// <see cref="UnitCompMoveSpeed"/> to determine possible circular interception if a U-turn is necessary. 
    /// </summary>
    /// <param name="position">Target position to set speed with respect to intercepting.</param>
    private void MoveToward_AdjustSpeed(Vector3 position)
    {
        if (aircraft.moveSpeedComponent == null || aircraft.rotateSpeedComponent == null) return;

        Vector3 directionToTarget;
        float angleToTarget;
        float angularSpeed;
        float movementSpeed;

        float interceptSpeedReq;
        float interceptSpeedDiff;
        float interceptAccelOrDecel;

        float circumference, diameter, radius;                              // Cirlce properties for circular interception.
        float lawOfCosAngle, lawOfCosSide1, lawOfCosSide2, lawOfCosSideX;   // Law of Cosines properties to validate circular interception point.

        directionToTarget = (position - transform.position).normalized;
        angleToTarget = Vector3.Angle(directionToTarget, transform.forward);
        angularSpeed = aircraft.rotateSpeedComponent.degrees;
        movementSpeed = aircraft.moveSpeedComponent.currentSpeed;

        /*
         * Calculate circle properties which is used to determine if the given position
         * can be intercepted if this aircraft has to move and rotate constantly to reach the position.
         */
        circumference = (360.0f / angularSpeed) * movementSpeed;
        diameter = circumference / Mathf.PI;
        radius = diameter / 2.0f;

        /*
         * Use Law of Cosines to determine how far the given position is away from the circle's origin,
         * which should also be the length of the radius as well.
         */
        lawOfCosAngle = (angleToTarget <= 90) ? 90 - angleToTarget : angleToTarget - 90;
        lawOfCosSide1 = radius;
        lawOfCosSide2 = Vector3.Distance(position, transform.position);

        lawOfCosSideX = Mathf.Sqrt(Mathf.Pow(lawOfCosSide1, 2.0f)
                                    + Mathf.Pow(lawOfCosSide2, 2.0f)
                                    - (2 * lawOfCosSide1 * lawOfCosSide2 * Mathf.Cos(lawOfCosAngle * Mathf.Deg2Rad)));

        /*
         * Calculate how much speed increase / decrease is needed to achieve the circular interception.
         * If the position is farther away than the radius then simply rotate and move towards it until
         * it is facing the target.
         */
        interceptSpeedReq = (lawOfCosSideX * 2 * Mathf.PI) / (360 / angularSpeed);
        interceptSpeedDiff = interceptSpeedReq - movementSpeed;
        interceptAccelOrDecel = 0.0f;

        if (interceptSpeedDiff > 0.0f) interceptAccelOrDecel = aircraft.moveSpeedComponent.maxAcceleration;
        if (interceptSpeedDiff < 0.0f) interceptAccelOrDecel = -(aircraft.moveSpeedComponent.maxDeceleration);

        interceptSpeedDiff = Mathf.Abs(interceptSpeedDiff);
        interceptAccelOrDecel *= Time.deltaTime;

        if (interceptAccelOrDecel > interceptSpeedDiff)
            interceptAccelOrDecel = (interceptAccelOrDecel / Mathf.Abs(interceptAccelOrDecel)) * interceptSpeedDiff;

        // MovementSpeedComponent.currentSpeed clamps between its maxSpeed.
        aircraft.moveSpeedComponent.currentSpeed += interceptAccelOrDecel;
    }

    /// <summary>Move in the forward direction using <see cref="UnitCompMoveSpeed.currentSpeed"/></summary>
    /// <param name="position">Target position to move forward to.</param>
    private void MoveToward_Forward(Vector3 position)
    {
        if (aircraft.moveSpeedComponent == null) return;
        transform.position += transform.forward * aircraft.moveSpeedComponent.currentSpeed * Time.deltaTime;
    }

    /// <summary>Rotate toward the position using <see cref="UnitCompRotateSpeed.degrees"/></summary>
    /// <param name="position">Target position to rotate toward to.</param>
    private void MoveToward_Rotate(Vector3 position)
    {
        if (aircraft.rotateSpeedComponent == null) return;

        float angularSpeed;
        Vector3 directionToTarget;
        Quaternion rotationToTarget;
        Quaternion newRotation;

        angularSpeed = aircraft.rotateSpeedComponent.degrees * Time.deltaTime;
        directionToTarget = position - transform.position;
        rotationToTarget = Quaternion.LookRotation(directionToTarget);

        newRotation = Quaternion.RotateTowards(transform.rotation, rotationToTarget, angularSpeed);

        newRotation = MoveToward_ApplyRigidbodyConstraintsRotation(newRotation);

        transform.rotation = newRotation;
    }

    /// <summary>Tilt/Bank this object around the Z-axis rotation when heading toward the given position.</summary>
    /// <param name="position">Target position to tilt/bank with respect to.</param>
    private void MoveToward_Tilt(Vector3 position)
    {
        if (aircraft.moveSpeedComponent == null || aircraft.rotateSpeedComponent == null || aircraft.moveTiltComponent == null) return;

        float currentSpeed;
        float maximumSpeed;
        float maxAngleTilt;
        float maxRateTilt;
        Vector3 newTilt;
        Quaternion newRotation;
        
        currentSpeed = aircraft.moveSpeedComponent.currentSpeed;
        maximumSpeed = aircraft.moveSpeedComponent.maxSpeed;
        maxAngleTilt = aircraft.moveTiltComponent.maxTiltAngleZ;
        maxRateTilt = aircraft.moveTiltComponent.maxRateOfTiltZ;
        
        newTilt = transform.rotation.eulerAngles;
        newTilt.z = Mathf.Clamp(Vector3.Angle(position - transform.position, transform.forward), 0.0f, maxAngleTilt);
        newTilt.z *= VectorJH3.LeftOrRight(transform.forward, (position - transform.position), transform.up);
        newTilt.z *= -1;    // Multiply -1 because eulerAngles left tilt is positive and right tilt is negative.
        
        newRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(newTilt), maxRateTilt * Time.deltaTime);
        
        newRotation = MoveToward_ApplyRigidbodyConstraintsRotation(newRotation);

        transform.rotation = newRotation;
        
    }

    /// <summary>
    /// Apply <see cref="RigidbodyConstraints"/> to the given <see cref="Quaternion"/>. Use this
    /// when direct modification is made to <see cref="Transform.rotation"/> since the constraints won't
    /// be applied as they're only handled through the <see cref="Rigidbody"/>.
    /// </summary>
    /// <param name="newRotation"></param>
    /// <returns></returns>
    private Quaternion MoveToward_ApplyRigidbodyConstraintsRotation(Quaternion newRotation)
    {
        if (aircraft.rigidbody == null) return newRotation;

        RigidbodyConstraints rigidbodyConstraints;
        float transRotX, transRotY, transRotZ;
        float newRotX, newRotY, newRotZ;

        rigidbodyConstraints = aircraft.rigidbody.constraints;
        transRotX = transform.rotation.eulerAngles.x;
        transRotY = transform.rotation.eulerAngles.y;
        transRotZ = transform.rotation.eulerAngles.z;
        newRotX = newRotation.eulerAngles.x;
        newRotY = newRotation.eulerAngles.y;
        newRotZ = newRotation.eulerAngles.z;

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationX) == RigidbodyConstraints.FreezeRotationX)
            newRotation = Quaternion.Euler(transRotX, newRotY, newRotZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationY) == RigidbodyConstraints.FreezeRotationY)
            newRotation = Quaternion.Euler(newRotX, transRotY, newRotZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationZ) == RigidbodyConstraints.FreezeRotationZ)
            newRotation = Quaternion.Euler(newRotX, newRotY, transRotZ);

        return newRotation;
    }

}

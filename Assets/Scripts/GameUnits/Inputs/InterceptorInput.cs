﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitCtrlFighterAircraft))]
public class InterceptorInput : MonoBehaviour {

    // Variables
    [Tooltip("Targeted GameUnit to fire at when in range.")]
    public GameUnit targetedUnit;                                   // Currently targeted GameUnit.
    [Tooltip("Positions to traverse.")]
    public List<Transform> movePositionList;                        // Transform positions for this object to traverse.

    private Bounds movePositionInterceptBounds;                     // Bounds to check if movePositionList positions have been intercepted.
    private UnitCtrlFighterAircraft fighterAirCraftController;    // Required FighterAircraftController to manipulate this object.

    
    // Methods
    private void Start()
    {
        fighterAirCraftController = GetComponent<UnitCtrlFighterAircraft>();
        if (fighterAirCraftController == null)
            fighterAirCraftController = gameObject.AddComponent<UnitCtrlFighterAircraft>();

        Aircraft aircraft = GetComponentInChildren<Aircraft>();

        movePositionInterceptBounds = new Bounds();

        if (aircraft != null)
            foreach (Collider collider in aircraft.bodyColliders)
                movePositionInterceptBounds.Encapsulate(collider.bounds);
        
        movePositionInterceptBounds.center = transform.position;
        movePositionInterceptBounds.size = movePositionInterceptBounds.size * (1.0f / 3.0f);
    }

    private void Update()
    {
        HandlingMoving();
        HandleUnitTargeting();
        HandleAttackingUnit();
    }

    /// <summary>
    /// Handles traversing the <see cref="Vector3"/> positions in <see cref="movePositionList"/>.
    /// If the <see cref="movePositionList"/> is empty then this unit will simply move forward.
    /// </summary>
    private void HandlingMoving()
    {
        if (movePositionList.Count > 0)
        {
            fighterAirCraftController.MoveToward(movePositionList[0].position);
            movePositionInterceptBounds.center = transform.position;

            if (movePositionInterceptBounds.Contains(movePositionList[0].position))
                movePositionList.RemoveAt(0);
        }
        else
        {
            fighterAirCraftController.MoveForward();
        }
    }

    /// <summary>
    /// Handles finding and targeting an enemy in sight range. This makes use of
    /// <see cref="UnitCtrlFighterAircraft.GetTargetInSightRange"/>, 
    /// <see cref="UnitCtrlFighterAircraft.IsTargetInSightRange(GameUnit)"/>,
    /// and <see cref="UnitCtrlFighterAircraft.MoveToward(GameUnit)"/>.
    /// </summary>
    private void HandleUnitTargeting()
    {
        if (targetedUnit == null || !fighterAirCraftController.IsTargetInSightRange(targetedUnit))
            targetedUnit = fighterAirCraftController.GetTargetInSightRange();
        
        if (targetedUnit != null)
            fighterAirCraftController.MoveToward(targetedUnit);
    }

    /// <summary>
    /// Fires <see cref="Weapon"/> from <see cref="Aircraft.weaponList"/> when the 
    /// <see cref="targetedUnit"/> is directly ahead and in range.
    /// </summary>
    private void HandleAttackingUnit()
    {
        if (targetedUnit == null) return;

        float distanceToTarget = fighterAirCraftController.IsTargetDirectlyAhead(targetedUnit);

        if (distanceToTarget >= 0.0f && distanceToTarget <= fighterAirCraftController.aircraft.sightRangeComponent.maxDistance)
            fighterAirCraftController.FireWeapons();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Jimmy He
 * 7/11/2017
 * JH3 - Aerial Combat
 */

/// <summary>
/// <see cref="ScoutInput"/> is made for the Aarcodian Scout <see cref="Aircraft"/>
/// <see cref="GameUnit"/>. Its purpose is only to traverse the positions in its
/// <see cref="ScoutInput.movePositionList"/> and do nothing else.
/// </summary>
[RequireComponent(typeof(UnitCtrlFighterAircraft))]
public class ScoutInput : AbstractInput {
    
    [Tooltip("Positions to traverse.")]
    public List<Transform> movePositionList;                        // Transform positions for this object to traverse.
    
    private Bounds movePositionInterceptBounds;                     // Bounds to check if movePositionList's positions have been intersected.
    private UnitCtrlFighterAircraft fighterAirCraftController;    // Required FighterAircraftController to manipulate this object.


    private void Start()
    {
        fighterAirCraftController = GetComponent<UnitCtrlFighterAircraft>();
        if (fighterAirCraftController == null)
            fighterAirCraftController = gameObject.AddComponent<UnitCtrlFighterAircraft>();

        Renderer[] renderers = GetComponentsInChildren<Renderer>();

        foreach (Renderer x in renderers)
            movePositionInterceptBounds.Encapsulate(x.bounds);

        movePositionInterceptBounds.center = transform.position;
        movePositionInterceptBounds.size = movePositionInterceptBounds.size * (1.0f/3.0f);
    }

    private void Update()
    {
        Update_HandleMovePosition();
    }

    private void Update_HandleMovePosition()
    {
        if (movePositionList.Count == 0) return;

        
        fighterAirCraftController.MoveToward(movePositionList[0].position);
        movePositionInterceptBounds.center = transform.position;

        if (movePositionInterceptBounds.Contains(movePositionList[0].position))
            movePositionList.RemoveAt(0);
        
    }
    
}

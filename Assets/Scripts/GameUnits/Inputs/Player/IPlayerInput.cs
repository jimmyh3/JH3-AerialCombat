﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerInput {

    /// <summary>
    /// Call any methods for handling inputs in this method and
    /// then place this method in the Update method.
    /// </summary>
    void HandleInput();

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

/**
 * Jimmy He
 * Created: 5/23/17
 * Package: Inputs
 */

/// <summary> 
/// PlayerInput receives input from the user directly and translates it to movement.
/// </summary>

public class PlayerInput : AbstractInput, IPlayerInput {
    
    // Variables
    private Vector3 newMousePosition;   // current position to move the Player to.
    private IPlayerController iPlayerController;

    // **********************************
    // 
    //          Unity Methods
    //
    // **********************************

    private void OnValidate()
    {
        if (transform.position.y != 0.0f)
        {
            transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
            Debug.Log(this.GetType().Name + " has set the Transform Y position to zero as needed for motion.");
        }
    }

    private void Start()
    {
        newMousePosition = transform.position;
        iPlayerController = GetComponent<IPlayerController>();
        OnValidate();
    }

    private void Update()
    {
        HandleInput();
    }

    private void FixedUpdate()
    {
        if (iPlayerController != null)
            iPlayerController.HandleMouseClick(newMousePosition);
    }

    // **********************************
    // 
    //      Public/Protected Methods
    //
    // **********************************

    public void HandleInput()
    {
        if (CrossPlatformInputManager.GetButton(InputJH3.LeftMouseKey))
        {
            // Set player movement destination to the mouse position upon clicking.
            newMousePosition = Input.mousePosition;
            newMousePosition.z = Camera.main.transform.position.y;  //Camera is at an Y offset from 0.
            newMousePosition = Camera.main.ScreenToWorldPoint(newMousePosition);
        }
    }
    
}

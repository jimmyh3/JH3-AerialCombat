﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Turret))]
public class TurretInput : MonoBehaviour {


    // Variables
    [Tooltip("Targeted GameUnit to fire at when in range.")]
    public GameUnit targetedUnit;                                   // Currently targeted GameUnit.
    private UnitCtrlFighterTurret fighterTurretController;        // Required controller to manipulate this turret.


    private void OnValidate()
    {
        fighterTurretController = GetComponent<UnitCtrlFighterTurret>();
        if (fighterTurretController == null)
        {
            Debug.LogWarning(this.GetType().Name + ": FighterTurretController was missing but has been added!", this);
            fighterTurretController = gameObject.AddComponent<UnitCtrlFighterTurret>();
        }
    }

    // Methods
    private void Start()
    {
        OnValidate();
    }

    private void Update()
    {
        HandleUnitTargeting();
        HandleAttackingUnit();
    }

    /// <summary>
    /// Handles finding and targeting an enemy in sight range. This makes use of
    /// <see cref="UnitCtrlFighterTurret.GetTargetInSightRange"/>, 
    /// <see cref="UnitCtrlFighterTurret.IsTargetInSightRange(GameUnit)"/>.
    /// </summary>
    private void HandleUnitTargeting()
    {
        if (targetedUnit == null || !fighterTurretController.IsTargetInSightRange(targetedUnit))
            targetedUnit = fighterTurretController.GetTargetInSightRange();
    }

    /// <summary>
    /// Fires <see cref="Weapon"/> from <see cref="Aircraft.weaponList"/> when the 
    /// <see cref="targetedUnit"/> is directly ahead and in range.
    /// </summary>
    private void HandleAttackingUnit()
    {
        if (targetedUnit == null) return;

        float distanceToTarget = fighterTurretController.IsTargetDirectlyAhead(targetedUnit);

        if (distanceToTarget >= 0.0f)
            fighterTurretController.FireWeapons();
        else
            fighterTurretController.RotateToward(targetedUnit);
    }

}

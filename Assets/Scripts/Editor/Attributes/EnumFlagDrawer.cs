﻿using System;
using System.Text;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EnumFlagAttribute flagSettings = (EnumFlagAttribute)attribute;
        Enum targetEnum = GetBaseProperty<Enum>(property);

        string propName = flagSettings.enumName;
        if (string.IsNullOrEmpty(propName))
            propName = GetFormalPropertyName(property.name);

        EditorGUI.BeginProperty(position, label, property);
        Enum enumNew = EditorGUI.EnumMaskField(position, propName, targetEnum);
        property.intValue = (int)Convert.ChangeType(enumNew, targetEnum.GetType());
        EditorGUI.EndProperty();
    }

    static T GetBaseProperty<T>(SerializedProperty prop)
    {
        // Separate the steps it takes to get to this property
        string[] separatedPaths = prop.propertyPath.Split('.');

        // Go down to the root of this serialized property
        System.Object reflectionTarget = prop.serializedObject.targetObject as object;
        // Walk down the path to get the target object
        foreach (var path in separatedPaths)
        {
            FieldInfo fieldInfo = reflectionTarget.GetType().GetField(path, BindingFlags.Public 
                                                                            | BindingFlags.NonPublic 
                                                                            | BindingFlags.Instance);
            reflectionTarget = fieldInfo.GetValue(reflectionTarget);
        }
        return (T)reflectionTarget;
    }

    private string GetFormalPropertyName(string name)
    {
        StringBuilder formalName = new StringBuilder(name);

        // Remove any non-letters until first character found.
        for (int i = 0; i < formalName.Length; i++)
        {
            if (Char.IsLetter(name[i]))
            {
                formalName.Remove(0, i);
                formalName[0] = Char.ToUpper(formalName[0]);
                break;
            }
        }

        // Add spaces before every capital letter
        for (int i = 1; i < formalName.Length-1; i++)
        {
            char curr = formalName[i];
            char next = formalName[i + 1];
            bool currIsLow = (Char.IsLetter(curr) && Char.IsLower(curr));
            bool nextIsUp = (Char.IsLetter(next) && Char.IsUpper(next));

            if (currIsLow && nextIsUp)
                formalName.Insert(i + 1, ' ');
        }

        return formalName.ToString();
    }
}

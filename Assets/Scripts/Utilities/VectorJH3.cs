﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorJH3 {

    /// <summary>
    /// Determine if a target is to the left or right. -1 = Left, 1 = Right, 0 = Forward/Behind. An example use would be
    /// <see cref="LeftOrRight(transform.forward, (target.position - transform.position), transform.up)"/>.
    /// </summary>
    /// <param name="faceFwdDirection">The main direction to determine if target is left or right of.</param>
    /// <param name="targetDirection">The direction to the target.</param>
    /// <param name="faceUpDirection">The direction perpendicular to the main direction.</param>
    /// <returns>-1 = Left; 1 = Right; 0 = Forward/Behind</returns>
    public static float LeftOrRight(Vector3 faceFwdDirection, Vector3 targetDirection, Vector3 faceUpDirection)
    {
        Vector3 cross = Vector3.Cross(faceFwdDirection, targetDirection);
        float dir = Vector3.Dot(cross, faceUpDirection);

        if (dir > 0f)
            return 1f;
        else if (dir < 0f)
            return -1f;
        else
            return 0f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PhysicsJH3 {

    private static string Ignore_Raycast = "Ignore Raycast";
    private static Dictionary<int, int> originalLayer = new Dictionary<int, int>();

    public static void SetToIgnoreLayer(GameObject gameObj)
    {
        SetToIgnoreLayer(gameObj, false);
    }

    public static void SetToIgnoreLayer(GameObject gameObj, bool forAllChildren)
    {
        if (forAllChildren)
        {
            foreach (Transform curTransform in gameObj.GetComponentsInChildren<Transform>(true))
            {
                originalLayer[curTransform.gameObject.GetInstanceID()] = curTransform.gameObject.layer;
                curTransform.gameObject.layer = LayerMask.NameToLayer(Ignore_Raycast);
            }
        }
        else
        {
            originalLayer.Add(gameObj.GetInstanceID(), gameObj.layer);
            gameObj.layer = LayerMask.NameToLayer(Ignore_Raycast);
        }
    }

    public static void ResetFromIgnoreLayer(GameObject gameObj)
    {
        ResetFromIgnoreLayer(gameObj, false);
    }

    public static void ResetFromIgnoreLayer(GameObject gameObj, bool forAllChildren)
    {
        int originalLayerID;

        if (forAllChildren)
        {
            foreach (Transform curTransform in gameObj.GetComponentsInChildren<Transform>(true))
            {
                if (originalLayer.TryGetValue(curTransform.gameObject.GetInstanceID(), out originalLayerID))
                {
                    curTransform.gameObject.layer = originalLayerID;
                    originalLayer.Remove(curTransform.gameObject.GetInstanceID());
                }
            }
        }
        else
        {
            if (originalLayer.TryGetValue(gameObj.GetInstanceID(), out originalLayerID))
            {
                gameObj.layer = originalLayerID;
                originalLayer.Remove(gameObj.GetInstanceID());
            }
        }
    }

}

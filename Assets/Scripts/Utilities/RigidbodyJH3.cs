﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyJH3 : MonoBehaviour {

    /// <summary>
    /// Apply <see cref="RigidbodyConstraints"/> to the given <see cref="Quaternion"/>. Use this
    /// when direct modification is made to <see cref="Transform.rotation"/> since the constraints won't
    /// be applied as they're only handled through the <see cref="Rigidbody"/>.
    /// </summary>
    /// <param name="newRotation"></param>
    /// <returns></returns>
    public static Quaternion GetManuallyAppliedRBConstraintsRotation(GameObject rigidbodyObject, Quaternion newRotation)
    {
        if (rigidbodyObject == null || rigidbodyObject.GetComponent<Rigidbody>() == null) return newRotation;

        RigidbodyConstraints rigidbodyConstraints;
        float transRotX, transRotY, transRotZ;
        float newRotX, newRotY, newRotZ;

        rigidbodyConstraints = rigidbodyObject.GetComponent<Rigidbody>().constraints;
        transRotX = rigidbodyObject.transform.rotation.eulerAngles.x;
        transRotY = rigidbodyObject.transform.rotation.eulerAngles.y;
        transRotZ = rigidbodyObject.transform.rotation.eulerAngles.z;
        newRotX = newRotation.eulerAngles.x;
        newRotY = newRotation.eulerAngles.y;
        newRotZ = newRotation.eulerAngles.z;

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationX) == RigidbodyConstraints.FreezeRotationX)
            newRotation = Quaternion.Euler(transRotX, newRotY, newRotZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationY) == RigidbodyConstraints.FreezeRotationY)
            newRotation = Quaternion.Euler(newRotX, transRotY, newRotZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationZ) == RigidbodyConstraints.FreezeRotationZ)
            newRotation = Quaternion.Euler(newRotX, newRotY, transRotZ);

        return newRotation;
    }


    /// <summary>
    /// Apply <see cref="RigidbodyConstraints"/> to the given <see cref="Vector3"/>. Use this
    /// when direct modification is made to <see cref="Transform.position"/> since the constraints won't
    /// be applied as they're only handled through the <see cref="Rigidbody"/>.
    /// </summary>
    /// <param name="newRotation"></param>
    /// <returns></returns>
    public static Vector3 GetManuallyAppliedRBConstraintsPosition(GameObject rigidbodyObject, Vector3 newPosition)
    {
        if (rigidbodyObject == null || rigidbodyObject.GetComponent<Rigidbody>() == null) return newPosition;

        RigidbodyConstraints rigidbodyConstraints;
        float transPosX, transPosY, transPosZ;
        float newPosX, newPosY, newPosZ;

        rigidbodyConstraints = rigidbodyObject.GetComponent<Rigidbody>().constraints;
        transPosX = rigidbodyObject.transform.position.x;
        transPosY = rigidbodyObject.transform.position.y;
        transPosZ = rigidbodyObject.transform.position.z;
        newPosX = newPosition.x;
        newPosY = newPosition.y;
        newPosZ = newPosition.z;

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationX) == RigidbodyConstraints.FreezeRotationX)
            newPosition = new Vector3(transPosX, newPosY, newPosZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationY) == RigidbodyConstraints.FreezeRotationY)
            newPosition = new Vector3(newPosX, transPosY, newPosZ);

        if ((rigidbodyConstraints & RigidbodyConstraints.FreezeRotationZ) == RigidbodyConstraints.FreezeRotationZ)
            newPosition = new Vector3(newPosX, newPosY, transPosZ);

        return newPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Implement this <see cref="ObserverJH3"/> so that it may be added by an
/// <see cref="ObservableJH3"/> object. Implement <see cref="ObserverJH3.OnNotify(ObservableJH3, object)"/>
/// according to the <see cref="ObservableJH3"/> it will be attached to so it may handle the receiving 
/// data correctly.
/// </summary>
public interface ObserverJH3 {

    void OnNotify(object caller, object arg);

}

﻿using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Extend this <see cref="ObservableJH3"/> class to gain access to 
/// notification methods to notify <see cref="ObserverJH3"/>s.
/// <para>
/// To properly make use <see cref="ObservableJH3"/> would require you to create a wrapper class
/// since most of Unity's classes require extending <see cref="UnityEngine.MonoBehaviour"/>.
/// Therefore the solution is to have a wrapper class extending <see cref="ObservableJH3"/>
/// and have it take in the <see cref="UnityEngine.MonoBehaviour"/> object you wish to observe.
/// The container class will modify it on its behalf.
/// </para>
/// </summary>
public class ObservableJH3 {

    List<ObserverJH3> observers = new List<ObserverJH3>();

    public void AddObserver(ObserverJH3 observer)
    {
        if (!observers.Contains(observer))
            observers.Add(observer);
    }

    public void RemoveObserver(ObserverJH3 observer)
    {
        observers.Remove(observer);
    }

    public void Notify()
    {
        Notify(null);
    }

    public void Notify(object arg)
    {
        foreach (ObserverJH3 observer in observers)
            observer.OnNotify(this, arg);
    }
}

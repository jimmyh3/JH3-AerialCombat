﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolJH3 : MonoBehaviour {

    // **** Variables ****
    public GameObject pooledObject;
    public int maxPoolSize = 20;
    public bool isPoolGrowable = true;

    private List<GameObject> pooledObjects;


    // **** Methods ****
    private void Start()
    {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < maxPoolSize; i++)
        {
            GameObject gobj = (GameObject)Instantiate(pooledObject);
            gobj.SetActive(false);
            pooledObjects.Add(gobj);
        }
    }

    /// <summary>Return an available pooled object, otherwise it returns null.</summary>
    /// <returns><see cref="GameObject"/></returns>
    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            // Replace dead reference to new instantiation
            if (pooledObjects[i] == null)
            {
                GameObject gobj = (GameObject)Instantiate(pooledObject);
                gobj.SetActive(false);
                pooledObjects[i] = gobj;
            }

            // Return first instance of deactivated object.
            if (!pooledObjects[i].activeInHierarchy)
                return pooledObjects[i];
        }

        if (isPoolGrowable)
        {
            GameObject gobj = (GameObject)Instantiate(pooledObject);
            gobj.SetActive(false);
            pooledObjects.Add(gobj);
            return gobj;
        }

        return null;
    }
}

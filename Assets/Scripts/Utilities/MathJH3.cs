﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathJH3 {

    /// <summary>Similar to <see cref="Mathf.Approximately(float, float)"/> but with precision input.</summary>
    /// <param name="a">First float value to compare against the second.</param>
    /// <param name="b">Second float value to compare against the first.</param>
    /// <param name="threshold">The precision in checking the given floats to consider them equals.</param>
    /// <returns></returns>
    public static bool FloatApproximately(float a, float b, float threshold)
    {
        return Mathf.Abs(a - b) < threshold;
    }

}

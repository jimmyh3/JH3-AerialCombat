﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIJH3 {

    /// <summary>
    /// Returns a <see cref="Rect"/> based on the <see cref="Bounds"/> of the given <see cref="GameObject"/>
    /// that is fitted for a <see cref="Screen"/> view. In other words, this calculates a rectangle that
    /// encompasses the given object. If the returned <see cref="Rect"/> is of zero width and height then it's
    /// the same as null.
    /// </summary>
    /// <param name="unit"><see cref="GameObject"/> to get the <see cref="Bounds"/> from.</param>
    /// <returns><see cref="Rect"/> that encompasses the given <see cref="GameObject"/> on a screen.</returns>
    public static Rect GetGUIRectCovering(GameUnit unit)
    {
        if (unit.bodyColliders.Count == 0) return new Rect(0, 0, 0, 0);

        Bounds bounds = unit.bodyColliders[0].bounds;
        Vector3[] pts = new Vector3[8];
        Camera mainCamera = Camera.main;

        // Encapsulate all the bounds of the given unit.
        foreach (Collider collider in unit.bodyColliders)
            bounds.Encapsulate(collider.bounds);

        //The object is behind us
        if (mainCamera.WorldToScreenPoint(bounds.center).z < 0) return new Rect(0, 0, 0, 0);

        //All 8 vertices of the bounds
        pts[0] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z + bounds.extents.z));
        pts[1] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z - bounds.extents.z));
        pts[2] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z + bounds.extents.z));
        pts[3] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z - bounds.extents.z));
        pts[4] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z + bounds.extents.z));
        pts[5] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z - bounds.extents.z));
        pts[6] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z + bounds.extents.z));
        pts[7] = mainCamera.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z - bounds.extents.z));

        //Get them in GUI space
        for (int i = 0; i < pts.Length; i++) pts[i].y = Screen.height - pts[i].y;

        //Calculate the min and max positions
        Vector3 min = pts[0];
        Vector3 max = pts[0];
        for (int i = 1; i < pts.Length; i++)
        {
            min = Vector3.Min(min, pts[i]);
            max = Vector3.Max(max, pts[i]);
        }

        //Construct a rect of the min and max positions and apply some margin
        Rect r = Rect.MinMaxRect(min.x, min.y, max.x, max.y);

        return r;

        ////Render the box
        //if (texture == null)
        //    GUI.Box(r, GUIContent.none);
        //else
        //    GUI.DrawTexture(r, texture, ScaleMode.StretchToFill);
    }
}

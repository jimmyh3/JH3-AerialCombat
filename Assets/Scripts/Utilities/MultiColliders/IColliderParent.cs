﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To be used in tandem with <see cref="ColliderChild"/>. The object containing this
/// <see cref="IColliderParent"/> should be a parent object to the <see cref="ColliderChild"/>.
/// The <see cref="ColliderChild"/> will search up its hierarchy and call
/// <see cref="IColliderParent.ColliderChildEnter(Collider, Collider)"/> to pass itself and its
/// associated collided object.
/// </summary>
public interface IColliderParent {

    void ColliderChildEnter(Collider child, Collider other);

    void ColliderChildExit(Collider child, Collider other);

}

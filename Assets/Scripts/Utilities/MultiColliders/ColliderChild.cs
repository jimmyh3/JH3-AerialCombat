﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// To be used in tandem with <see cref="IColliderParent"/>, which should be attached a
/// parent object in the hierarchy of this object. <see cref="ColliderChild"/> will
/// call its <see cref="IColliderParent"/> handler and pass itself and its associated collided target
/// via <see cref="IColliderParent.ColliderChildEnter(Collider, Collider)"/>.
/// </summary>
[DisallowMultipleComponent]
public class ColliderChild : MonoBehaviour {

    // ***** Private Fields *****
    private Collider _myCollider;
    private IColliderParent _myHandler;

    // ***** Properties *****
    public Collider myCollider
    {
        get
        {
            if (_myCollider == null)
                _myCollider = GetComponent<Collider>();

            return _myCollider;
        }
    }

    private IColliderParent myHandler
    {
        get
        {
            if (_myHandler == null)
                _myHandler = GetComponentInParent<IColliderParent>();

            return _myHandler;
        }
    }

    // ***** Unity Methods *****
    private void OnValidate()
    {
        if (myCollider == null)
            Debug.LogWarning(this.GetType().Name + ": collider on this object cannot be found!", this);
    }

    private void Awake()
    {
        OnValidate();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (myCollider != null && myHandler != null)
        {
            myHandler.ColliderChildEnter(myCollider, other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (myCollider != null && myHandler != null)
        {
            myHandler.ColliderChildExit(myCollider, other);
        }
    }
}

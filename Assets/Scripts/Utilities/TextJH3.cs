﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextJH3 : Text {
    
    // ***** Private Fields *****
    /// <summary> <see cref="Coroutine"/> to run <see cref="FlashAlphaText(Text, float, float, bool)"/>.</summary>
    private IEnumerator flashAlphaCoroutine;
    
    
    // *************************
    //
    //      Public Methods
    //
    // *************************

    /// <summary>
    /// Flashes the text using <see cref="PlayerPowersDisplay.messageText"/> and its <see cref="Color.a"/>,
    /// which is its alpha color. After the duration ends the <see cref="Color.a"/> will be reset to its original value.
    /// </summary>
    /// <param name="textToDisplay">The <see cref="string"/> to display.</param>
    /// <param name="flashRate">The rate of flashing its alpha per second. This is considered on a 0 to 255 scale.</param>
    /// <param name="duration">The duration of flashing before ending.</param>
    public void FlashAlphaText(string textToDisplay, float flashRate, float duration)
    {
        if (textToDisplay.Equals("")
                || Mathf.Approximately(flashRate, 0.0f)
                    || Mathf.Approximately(duration, 0.0f)) return;

        // Stop previous Coroutine if it was called before.
        if (flashAlphaCoroutine != null)
            StopCoroutine(flashAlphaCoroutine);
        
        this.text = textToDisplay;

        // Use 'this' as the argument because method signatures don't differ for IEnumerators
        flashAlphaCoroutine = FlashAlphaText(this, flashRate, duration);

        StartCoroutine(flashAlphaCoroutine);
    }
    
    private IEnumerator FlashAlphaText(Text textObject, float flashRate, float duration)
    {
        float timeElapsed = 0.0f;
        float savedAlphaInc255 = 0.0f;

        float r, g, b, a;
        r = textObject.color.r;
        g = textObject.color.g;
        b = textObject.color.b;
        a = textObject.color.a;
        savedAlphaInc255 = textObject.color.a * 255.0f;

        textObject.gameObject.SetActive(true);

        // Flash the alpha of the text object for the given duration by the given rate.
        while (timeElapsed < duration)
        {
            savedAlphaInc255 += (flashRate * Time.unscaledDeltaTime);

            textObject.color = new Color(r, g, b, (Mathf.PingPong(savedAlphaInc255, 255.0f) / 255.0f));

            timeElapsed += Time.unscaledDeltaTime;
            yield return null;
        }

        textObject.color = new Color(r, g, b, a);
    }

}

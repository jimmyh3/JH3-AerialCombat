﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This class only has a reference to a <see cref="AbstractPower"/>. This is used
/// in cases where the <see cref="GameObject"/> this component resides on is constantly activated/deactivated
/// but the <see cref="AbstractPower"/> itself should not be. In other words, the <see cref="AbstractPower"/>
/// should not be on the same <see cref="GameObject"/> as this <see cref="PowerReference"/> is on and
/// instead, should be referenced by the <see cref="PowerReference"/>.
/// </summary>
[DisallowMultipleComponent]
public class PowerReference : MonoBehaviour {
    
    [SerializeField]
    [Tooltip("Drag the associated power to be found here.")]
    public AbstractPower power;
    

    private void Awake()
    {
        if (power == null)
            Debug.LogError(this.name + ": playerPower is missing!");
    }


//#if UNITY_EDITOR
//    [CustomEditor(typeof(PlayerPowerReference))]
//    [CanEditMultipleObjects]
//    private class PlayerPowerReferenceEditor : Editor
//    {
//        public override void OnInspectorGUI()
//        {
//            base.OnInspectorGUI();
//        }
//    }
//#endif
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PowersManager : MonoBehaviour {

    // ****** Class Fields ******
    /// <summary>The central Player object of which these powers belong to and to originate from.</summary>
    public List<AbstractPower> powersList;

    // *************************
    //
    //      Unity Methods
    //
    // *************************  

    private void Awake()
    {
        // Add any powers to the list that are nested in this manager's game object.
        foreach (AbstractPower app in this.GetComponentsInChildren<AbstractPower>(true))
        {
            if (!powersList.Contains(app))
                powersList.Add(app);
        }
    }
    

    // *************************
    //
    //      Public Methods
    //
    // *************************    
    
    public List<AbstractPower> GetActivePowers()
    {
        List<AbstractPower> activePowers = new List<AbstractPower>();

        foreach (AbstractPower power in powersList)
            if (power.isActivated)
                activePowers.Add(power);

        return activePowers;
    }

    public bool EnablePower<T>() where T : AbstractPower
    {
        foreach (AbstractPower power in powersList)
        {
            if (power.GetType().IsAssignableFrom(typeof(T)))
            {
                power.gameObject.SetActive(true);
                power.enabled = true;
                return true;
            }
        }

        return false;
    }

    public void EnablePowerAll<T>() where T : AbstractPower
    {
        foreach (AbstractPower power in powersList)
        {
            power.gameObject.SetActive(true);
            power.enabled = true;
        }
    }

    public bool DisablePower<T>() where T : AbstractPower
    {
        foreach (AbstractPower power in powersList)
        {
            if (power.GetType().IsAssignableFrom(typeof(T)))
            {
                power.gameObject.SetActive(false);
                power.enabled = false;
                return true;
            }
        }

        return false;
    }

    public void DisablePowerAll()
    {
        foreach (AbstractPower power in powersList)
        {
            power.gameObject.SetActive(false);
            power.enabled = false;
        }
    }
    
}

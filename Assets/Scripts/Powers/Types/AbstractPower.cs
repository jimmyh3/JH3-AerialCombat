﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class AbstractPower : MonoBehaviour {

    //public enum Phases { Targeting, Activated, None }

    //  ****** Fields ******
    protected GameUnit powerWielder;
    public List<GameUnit> targetedUnits = new List<GameUnit>();

    // ****** Properties ******
    /// <summary>
    /// Status this <see cref="AbstractPower"/>. 
    /// Use <see cref="AbstractPower.TryActivatingAffect(GameUnit, out string)"/> to turn on this power.
    /// </summary>
    public virtual bool isActivated { get; protected set; }
    //public bool isTargeting { get; set; }

    // *************************
    //
    //      Unity Methods
    //
    // *************************

    protected void Update()
    {
        if (isActivated)
            AffectTargetedUnits(targetedUnits); // Implement this method. Leave Update() alone.
    }

    /// <summary>
    /// Affect the <see cref="GameUnit"/>s in <see cref="targetedUnits"/>.
    /// <para>
    /// Note that this method will do nothing unless it's in its <see cref="isActivated"/> phase.
    /// Use <see cref="TryActivatingAffect(GameUnit, out string)"/> to turn it on.
    /// </para>
    /// </summary>
    /// <param name="powerWielder">The <see cref="GameUnit"/> of which this power belongs to.</param>
    public abstract void AffectTargetedUnits(List<GameUnit> targetedUnits);

    /// <summary>Reset the values of this <see cref="AbstractPower"/>.</summary>
    public abstract void ResetAffects();

    /// <summary>
    /// Get a <see cref="List{T}"/> of potential <see cref="GameUnit"/> targets based on the
    /// given <see cref="GameUnit"/> <see cref="AbstractPower"/> wielder's
    /// <see cref="UnitCompFriendFoe.faction"/>. The number of targets allowed are also given.
    /// The returning <see cref="bool"/> indicates if any targets are required at all.
    /// </summary>
    /// <param name="powerWielder">The owner of this <see cref="AbstractPower"/>.</param>
    /// <param name="potentialTargets"><see cref="List{T}"/> of potential targets to choose from.</param>
    /// <param name="numOfTargets">The number of targets allowed.</param>
    /// <returns><see cref="bool"/> determining if targets are even required.</returns>
    public abstract bool TryGetPotentialTargets(GameUnit powerWielder, out List<GameUnit> potentialTargets, out int numOfTargets);
    
    /// <summary>
    /// Attempt to activate this <see cref="AbstractPower"/> assuming any/all
    /// of its constraints are met. The <see cref="out"/> parameter is a <see cref="string"/>
    /// indicating requirements to activate if they aren't met; the <see cref="string"/> will be
    /// <see cref="null"/> if successful, and likewise the returning <see cref="bool"/> will be true
    /// otherwise it will be false.
    /// </summary>
    /// <param name="constraintsUnmet">Detailing what constraints are required before using this power.</param>
    /// <returns><see cref="bool"/> indicating whether the power has been activated or not.</returns>
    public abstract bool TryActivatingAffect(GameUnit powerWielder, out string constraintsUnmet);

    /// <summary>
    /// Disables the <see cref="AbstractPower"/>'s <see cref="Behaviour.enabled"/> and 
    /// <see cref="GameObject.SetActive(bool)"/>. However it does not reset the targets that have been assigned.
    /// </summary>
    /// <returns></returns>
    public abstract bool TryDeactivatingAffect();
    
}

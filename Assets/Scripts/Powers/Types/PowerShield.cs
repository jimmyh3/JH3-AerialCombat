﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerShield : AbstractPower {

    // ***** Class fields *****
    [SerializeField] private GameObject shieldPrefab;
    [SerializeField] private float _shieldDuration = 5.0f;
    private GameObject _shieldInstance;
    private float shieldTimeElapsed;

    // ***** Properties *****
    public GameObject shieldInstance
    {
        get
        {
            if (_shieldInstance == null)
                _shieldInstance = Instantiate(shieldPrefab);

            // Always ensure the shield is on the target.
            _shieldInstance.transform.position = Vector3.zero;
            _shieldInstance.transform.localPosition = Vector3.zero;
            _shieldInstance.transform.rotation = Quaternion.identity;
            _shieldInstance.transform.localRotation = Quaternion.identity;

            return _shieldInstance;
        }
    }

    public float shieldDuration
    {
        get { return _shieldDuration; }
        set { _shieldDuration = Mathf.Clamp(value, 0.0f, float.MaxValue); }
    }

    // ************************
    //
    //      Unity Methods
    //
    // ************************

    private void OnValidate()
    {
        if (shieldPrefab == null)
            Debug.LogError(this.name + ": shieldPrefab is missing!", this);
        
        shieldDuration = shieldDuration;
    }

    private void Awake()
    {
        OnValidate();

        shieldTimeElapsed = 0.0f;
        _shieldInstance = Instantiate(shieldPrefab);
        _shieldInstance.gameObject.SetActive(false);
        
    }

    // ************************
    //
    //      Public Methods
    //
    // ************************

    public override void AffectTargetedUnits(List<GameUnit> _targetedUnits)
    {
        if (isActivated && (powerWielder != null) && (shieldTimeElapsed < shieldDuration))
        {
            shieldTimeElapsed += Time.deltaTime;
        }
        else
        {
            ResetAffects();
        }
        
    }

    public override void ResetAffects()
    {
        HandleSetInvincibility(powerWielder, false);
        shieldInstance.transform.SetParent(this.gameObject.transform);
        shieldInstance.gameObject.SetActive(false);
        shieldTimeElapsed = 0.0f;
        isActivated = false;

        powerWielder = null;
        targetedUnits.Clear();
    }

    public override bool TryGetPotentialTargets(GameUnit powerWielder, out List<GameUnit> potentialTargets, out int numOfTargets)
    {
        potentialTargets = null;
        numOfTargets = 0;
        return false;
    }

    public override bool TryActivatingAffect(GameUnit _powerWielder, out string constraintsUnmet)
    {
        string response = "";
        bool status = false;

        if (_powerWielder == null)
        {
            response = "No power wielder!";
            status = false;
        }
        else if (isActivated)
        {
            response = "Power Shield is already active!";
            status = true;
        }
        else
        {
            response = "Power Shield activated!";

            this.powerWielder = _powerWielder;
            HandleSetInvincibility(_powerWielder, true);
            shieldInstance.transform.SetParent(_powerWielder.transform);
            shieldInstance.gameObject.SetActive(true);
            shieldTimeElapsed = 0.0f;
            isActivated = true;

            status = true;
        }

        constraintsUnmet = response;
        return status;
    }

    public override bool TryDeactivatingAffect()
    {
        HandleSetInvincibility(powerWielder, false);
        powerWielder = null;
        shieldTimeElapsed = 0.0f;
        isActivated = false;
        return true;
    }

    // **************************
    //
    //      Private Methods
    //
    // **************************

    /// <summary>
    /// Sets the <see cref="UnitCompHealth.isInvincibile"/> state of the given <see cref="GameUnit"/> if
    /// a <see cref="UnitCompHealth"/> exists on the object.
    /// </summary>
    /// <param name="unit">The <see cref="GameUnit"/> to find the <see cref="UnitCompHealth"/> on.</param>
    /// <param name="state">The state of the <see cref="UnitCompHealth.isInvincibile0"/> to set to.</param>
    private void HandleSetInvincibility(GameUnit unit, bool state)
    {
        if (unit == null) return;

        UnitCompHealth healthComponent = unit.GetComponent<UnitCompHealth>();
        if (healthComponent != null) healthComponent.isInvincibile = state;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.LightningBolt;
using UnityStandardAssets.CrossPlatformInput;

public class PowerElectricBolt : AbstractPower
{
    // ****** Fields ******
    [Tooltip("Texture to draw onto objects that can be targeted during targeting phase.")]
    public Texture isTargetableRect;
    [Tooltip("Texture to draw onto objects that has been targeted during targeting phase.")]
    public Texture isTargetedRect;
    [Tooltip("Electric bolt effect to instantiate from the Player to each selected target.")]
    public LightningBoltScript electricBoltPrefab;
    [Tooltip("Maximum number of selectable targets.")]
    public int maxTargetCount = 5;
    [Tooltip("Electric bolt duration until disappearing.")]
    public float electricBoltDuration = 3.0f;
    [Tooltip("Electric bolt damage per second.")]
    public float electricBoltDPS = 30.0f;

    private List<LightningBoltScript> electricBolts = new List<LightningBoltScript>();
    private float electricBoltElapsedTime;

    // ***********************
    //
    //      Unity Methods
    //
    // ***********************

    private void OnValidate()
    {
        if (electricBoltPrefab == null)
            Debug.LogError("Electric Bolt prefab is required!", this);
    }

    protected void Start()
    {
        OnValidate();

        for (int i = 0; i < maxTargetCount; i++)
            electricBolts.Add(HandleInstantiateSetupLightningBolt());
    }

    // *****************************************
    //
    //      Public/Protected Class Methods
    //
    // *****************************************
    
    public override void AffectTargetedUnits(List<GameUnit> _targetedUnits)
    {
        // Affect set selected targets.
        if (isActivated && (powerWielder != null) && (electricBoltElapsedTime < electricBoltDuration))
        {
            electricBoltElapsedTime += Time.deltaTime;
            
            // Always reset bolts to targets in case something intercepted the bolt before.
            HandleSetElectricBoltsToTargets(electricBolts, _targetedUnits);

            foreach (LightningBoltScript electricBolt in electricBolts)
            {
                // Do nothing if the origin or destination object is gone.
                if (!electricBolt.enabled || !electricBolt.gameObject.activeSelf
                        || electricBolt.StartObject == null || electricBolt.EndObject == null) continue;

                RaycastHit hitInfo;
                Vector3 ebStartPos = electricBolt.StartObject.transform.position;
                Vector3 ebEndPos = electricBolt.EndObject.transform.position;
                    
                // For line casting: set origin object to ignore self to avoid self collision.
                PhysicsJH3.SetToIgnoreLayer(electricBolt.StartObject, true);

                /*
                 * Linecast from power wielder to target to determine if bolt can reach each other uninterrupted.
                 *      If it is interrupted then the target destination will be swapped with the intercepting object.
                 */
                if (Physics.Linecast(ebStartPos, ebEndPos, out hitInfo))
                {
                    GameUnit hitGameUnit;
                    UnitCompHealth hitHealthComp;
                        
                    hitGameUnit = hitInfo.transform.GetComponent<GameUnit>();
                    hitHealthComp = (hitGameUnit != null) ? hitInfo.transform.GetComponent<UnitCompHealth>() : null;

                    // Apply damage if targeted object has a health component and is an enemy.
                    if (hitGameUnit != null && GameUnitFriendFoeManager.Instance.IsEnemy(powerWielder, hitGameUnit))
                    {
                        if (hitHealthComp != null)
                        {
                            hitHealthComp.ReceiveDamage(electricBoltDPS * Time.deltaTime);
                            if (hitHealthComp.currentHealth <= 0.0f)
                            {
                                HandleResetElectricBolt(electricBolt);
                                electricBolt.enabled = false;
                                electricBolt.gameObject.SetActive(false);
                            }
                        }
                    }

                    // If applicable: swap the original target destination of the bolt to the intercepting object.
                    if (electricBolt.EndObject != null && hitInfo.transform != electricBolt.EndObject.transform)
                        HandleSetElectricBolt(electricBolt, electricBolt.StartObject, Vector3.zero, null, hitInfo.point);
                            
                }

                // Reset origin object to its original layer.
                PhysicsJH3.ResetFromIgnoreLayer(electricBolt.StartObject, true);
            }
                
        }
        else
        {
            ResetAffects(); // Reset all data fields after elapsed time has passed the maximum duration.
        }
    }

    public override void ResetAffects()
    {
        foreach (LightningBoltScript electricBolt in electricBolts)
        {
            HandleResetElectricBolt(electricBolt);
            electricBolt.enabled = false;
            electricBolt.gameObject.SetActive(false);
        }

        isActivated = false;
        targetedUnits.Clear();
        electricBoltElapsedTime = 0.0f;
    }

    public override bool TryGetPotentialTargets(GameUnit powerWielder, out List<GameUnit> potentialTargets, out int numOfTargets)
    {
        if (powerWielder == null)
        {
            potentialTargets = null;
            numOfTargets = 0;
            return false;
        }
        else
        {
            GameUnitFriendFoeManager.Faction myFaction = powerWielder.idFriendFoeComponent.faction;
            potentialTargets = GameUnit.TrackedUnits.GetTrackedByFactionOpposition(myFaction);
            numOfTargets = maxTargetCount;

            return true;
        }
    }

    /// <summary>
    /// Attempt to activate this <see cref="AbstractPower"/> assuming any/all
    /// of its constraints are met. The <see cref="out"/> parameter is a <see cref="string"/>
    /// indicating requirements if they aren't met; the <see cref="string"/> will be null
    /// if successful.
    /// </summary>
    /// <param name="constraintsUnmet">Detailing what constraints are required before using this power.</param>
    /// <returns><see cref="bool"/> indicating whether the power has been activated or not.</returns>
    public override bool TryActivatingAffect(GameUnit _powerWielder, out string constraintsUnmet)
    {
        string response = "";
        bool status = false;

        if (_powerWielder == null)
        {
            constraintsUnmet = "No power wielder!";
            status = false;
        }
        else if (isActivated)
        {
            constraintsUnmet = "Electric Bolts are already active!";
            status = true;
        }
        else if (targetedUnits.Count <= 0)
        {
            response = "Select up to " + maxTargetCount + " targets!";
            status = false;
        }
        else
        {
            powerWielder = _powerWielder;
            HandleSetElectricBoltsToTargets(electricBolts, targetedUnits);

            response = typeof(PowerElectricBolt).Name + " Activated!";
            isActivated = true;
        }

        constraintsUnmet = response;

        return status;
    }

    /// <summary>Sets the <see cref="AbstractPower.isActivated"/> to false.</summary>
    /// <returns><see cref="bool"/> indicating if the deactivation was successful.</returns>
    public override bool TryDeactivatingAffect()
    {
        foreach (LightningBoltScript electricBolt in electricBolts)
        {
            electricBolt.enabled = false;
            electricBolt.gameObject.SetActive(false);
        }
        
        isActivated = false;

        return isActivated;
    }

    // *******************************
    //
    //      Private Methods
    //
    // *******************************
    
    /// <summary>
    /// Instantiates a <see cref="LightningBoltScript"/>, sets its <see cref="Transform.parent"/> to <see cref="this"/>,
    /// set its <see cref="GameObject.SetActive(bool)"/> to false, and then returns it.
    /// </summary>
    /// <returns></returns>
    private LightningBoltScript HandleInstantiateSetupLightningBolt()
    {
        LightningBoltScript electricBolt = Instantiate(electricBoltPrefab, this.transform);
        electricBolt.transform.SetParent(this.transform);
        electricBolt.gameObject.SetActive(false);

        return electricBolt;
    }
    
    /// <summary>
    /// Set every <see cref="LightningBoltScript"/> in <see cref="electricBolts"/> to targeted <see cref="GameUnit"/>s 1 to 1.
    /// For any references that are null will cause both to be deactivated via <see cref="GameObject.SetActive(false)"/>.
    /// </summary>
    private void HandleSetElectricBoltsToTargets(List<LightningBoltScript> _electricBolts, List<GameUnit> _targetedUnits)
    {
        // Set each bolt to affect a designated target and origin.
        for (int i = 0; i < _targetedUnits.Count; i++)
        {
            // Create another electric bolt if there are more targets than cached bolts.
            if (electricBolts.Count == i)
                electricBolts.Add(HandleInstantiateSetupLightningBolt());

            if (_targetedUnits[i] == null)
            {
                HandleResetElectricBolt(electricBolts[i]);
                electricBolts[i].enabled = false;
                electricBolts[i].gameObject.SetActive(false);
            }
            else
            {
                HandleSetElectricBolt(electricBolts[i], powerWielder.gameObject,
                                                    Vector3.zero,
                                                        _targetedUnits[i].gameObject,
                                                            Vector3.zero);
                electricBolts[i].enabled = true;
                electricBolts[i].gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Set the given <see cref="LightningBoltScript"/> according to the given values.
    /// </summary>
    /// <param name="electricBolt">The <see cref="LightningBoltScript"/> with its properties to modify.</param>
    /// <param name="startObject">Corresponds to <see cref="LightningBoltScript.StartObject"/>.</param>
    /// <param name="startPosition">Corresponds to <see cref="LightningBoltScript.StartPosition"/></param>
    /// <param name="endObject">Corresponds to <see cref="LightningBoltScript.EndObject"/></param>
    /// <param name="endPosition">Corresponds to <see cref="LightningBoltScript.EndPosition"/></param>
    private void HandleSetElectricBolt(LightningBoltScript electricBolt, GameObject startObject, 
                                                                    Vector3 startPosition, 
                                                                    GameObject endObject, 
                                                                    Vector3 endPosition     )
    {
        electricBolt.StartObject = startObject;
        electricBolt.StartPosition = startPosition;
        electricBolt.EndObject = endObject;
        electricBolt.EndPosition = endPosition;
    }

    /// <summary>
    /// Sets <see cref="LightningBoltScript.StartObject"/> and <see cref="LightningBoltScript.EndObject"/> to null.
    /// Sets <see cref="LightningBoltScript.StartPosition"/> and <see cref="LightningBoltScript.EndPosition"/> to <see cref="Vector3.zero"/>.
    /// </summary>
    /// <param name="electricBolt">The <see cref="LightningBoltScript"/> to reset values of.</param>
    private void HandleResetElectricBolt(LightningBoltScript electricBolt)
    {
        electricBolt.StartObject = null;
        electricBolt.StartPosition = Vector3.zero;
        electricBolt.EndObject = null;
        electricBolt.EndPosition = Vector3.zero;
    }


    // **************************
    //
    //      Helper Classes
    //
    // **************************
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameManagerJH3 : MonoBehaviour {

    // ***************************
    //
    //      Variables 
    //
    // ***************************

    [SerializeField] private PlayerUIManager _uiManager;
    [SerializeField] private AbstractLevelManager _levelManager;
    [SerializeField] private GameUnitFriendFoeManager _idFriendFoeManager;
    private static float defaultFixedDeltaTime;

    // ***************************
    //
    //      Properties 
    //
    // ***************************
    
    public PlayerUIManager uiManager
    {
        get
        {
            if (_uiManager == null)
                _uiManager = GameObject.FindObjectOfType<PlayerUIManager>();

            return _uiManager;
        }
    }

    // TODO: make Properties for the rest of the Managers


    private void OnValidate()
    {
        _uiManager = uiManager;
    }

    // Use this for initialization
    void Start () {
        // TODO: Find all relevant Managers to ensure they're on the scene.
        Time.timeScale = 1;
        defaultFixedDeltaTime = Time.fixedDeltaTime;
	}
	

    /// <summary>Pauses the game completely via <see cref="Time.timeScale"/>.</summary>
    public static void PauseGame()
    {
        Time.timeScale = 0;
        Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;
    }

    /// <summary>
    /// Slows down the game via the <see cref="Time.timeScale"/> value.
    /// The value is clamped between 0 and 1.
    /// </summary>
    /// <param name="timeScaleValue">The value to set <see cref="Time.timeScale"/></param>
    public static void PauseGame(float timeScaleValue)
    {
        Time.timeScale = Mathf.Clamp(timeScaleValue, 0.0f, 1.0f);
        Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;
    }

    /// <summary>Unpause the game by setting the <see cref="Time.timeScale"/> to 1.</summary>
    public static void UnPauseGame()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = defaultFixedDeltaTime * Time.timeScale;
    }

    /// <summary>
    /// Restarts the <see cref="Scene"/> by reloading the current <see cref="Scene"/>
    /// via <see cref="SceneManager.LoadScene(string)"/>.
    /// </summary>
    public static void RestartScene()
    {
        UnPauseGame();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    /// <summary>
    /// Exits the game by setting 
    /// <see cref="EditorApplication.isPlaying"/> to false.</summary>
    public static void QuitGame()
    {
        #if UNITY_EDITOR
            EditorApplication.isPlaying = false;
        #else
		    Application.Quit();
        #endif
    }
}
